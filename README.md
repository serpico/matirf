# Matirf
  Author: Jérôme Boulanger (jerome.boulanger@curie.fr)

## Description
   Multi-Angle Tirf Image reconstruction

   Reconstruct multi-angle tirf data.
   Acquisition parameters are stored in the TIFF comments or as a json files.

   example parameter file:
   {
   "wavelength_nm": 480,
   "glass_index": 1.52,
   "medium_index": 1.33,
   "oil_index": 1.52,
   "numerical_aperture": 1.42,
   "beam_divergence": 0.3,
   "angles":[61, 61.5, 68.4]
   }

## Compilation
   The built system is based on a Makefile.
   Type 'make release' to compile the package with optimizations.

## Usage

- Recontruction of a multi angle image stack
  ./matirf -i img.tif -p param.json -o rec.tif -idx idx.tif
  Index estimation
  use '-index min,max,n,scale' for multi-index estimation
  with '-algo 1' for pxxa reconstruction using the estimated index map
  with '-algo 3' for blind pxxa reconstruction using the estimated index map as initialization
  use '-index value' for using a single predefined index
  with '-algo 0' for nnls reconstruction using the given index
  with '-algo 1' for pxxa reconstruction using the given index
  with '-algo 2' for blind pxxa reconstruction initialized using the given index
  with '-algo 3' for blind adaptive pxxa reconstruction using the given index


- Reconstruction of an image sequence
  ./matirf sequence -i img%03d.tif -p param.json -o rec%03d.tif

- Meta data extraction
  TIFF metadata can be exported to json as:
  ./matirf metadata -i img.tif -o param.json

- Creation of metadata
  Metadata corresponding to an acquisition parameters can be generated using
  ./matirf generate -o param.json -w 491 -angles 61,80,10
  in order to get a regualar sampling in penetration depth use:
   ./matirf generate -o param.json -w 491 -depths 61,80,10

- Depth localization
  ./matirf localize -i img.tif -p param.json -c coords2d.tif -o coords3d.tif

- Model analysis
  Display the eigen values of the operator and the profile I(angle) for beads.
  ./matirf model -p param.json -o operator.dlm -b profiles.tif

- Simulation
  - XOS
  ./xos -d 10 -x xos_volume.tif
  gmic ../xos/xos_volume_000019.tif -a z -s c -+ -n 0,255 -type uchar -o ./data/xos/img.tif
  ./matirf simulate -i ./data/xos/img.tif -o ./data/xos/matirf.tif -p  ./data/xos/matirf.json  -d 500
  ./matirf -i ./data/xos/matirf.tif -o ./data/xos/matirf_rec.tif -p ./data/xos/matirf.json -index 1.34
  gmic -tiff3d  ./data/xos/img.tif -tiff3d ./data/xos/matirf_rec.tif -resize[1] [0] -a c

  - NUP

   gmic 64,64,500 -point 50%,50%,100,1,255 -point 50%,50%,300,1,255 -blur 1 -n 0,255 -type uchar -o model_nup.tif
