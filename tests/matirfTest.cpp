#include <matirf>
#include "matirfTestConfig.h"

#include <iostream>

int main(int argc, char *argv[])
{

    std::cout << "input sample = " << INPUTSAMPLE << std::endl;
    std::cout << "input parameter = " << INPUTPARAM << std::endl;
    std::cout << "output result = " << OUTPUTRES << std::endl;


    int margc = 21;
    char* margv[21];
    margv[0] = "matirf";
    margv[1] = "-i";
    margv[2] = INPUTSAMPLE;
    margv[3] = "-p";
    margv[4] = INPUTPARAM;
    margv[5] = "-o";
    margv[6] = OUTPUTRES;
    margv[7] = "-d";
    margv[8] = "500";
    margv[9] = "-n";
    margv[10] = "20";
    margv[11] = "-lambda";
    margv[12] = "0.75,0.75";
    margv[13] = "-gamma";
    margv[14] = "10";
    margv[15] = "-iter";
    margv[16] = "50";
    margv[17] = "-reg";
    margv[18] = "1";
    margv[19] = "-zmin";
    margv[20] = "80";

    // matirf -i CellRCL238E-050ms-Lag2s_Optovar1x5_001.tif -p CellRCL238E-050ms-Lag2s_Optovar1x5_001.json -o CellRCL238E-050ms-Lag2s_Optovar1x5_001RES_t001.tif -d 500 -n 20 -lambda 0.75,0.75 -gamma 10 -iter 50 -reg 1 -zmin 80

    return reconstruct(margc, margv);

}
