/**
 *
 * \file matirf.cpp
 *
 * \description Main multi-angle TIRF image reconstruction
 *
 * \section licence_sec Licence
 *
 * Copyright Jérôme Boulanger (2013)
 *
 * jerome.boulanger@curie.fr
 *
 * This software is a computer program whose purpose is to reconstruct
 * images acquired in multi-angle total internal reflection microscopy.
 *
 * This software is governed by the CeCILLlicense under French law and
 * abiding by the rules of distribution of free software.  You can use,
 * modify and/ or redistribute the software under the terms of the
 * CeCILL license as circulated by CEA, CNRS and INRIA at the following
 * URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to
 * copy, modify and redistribute granted by the license, users are
 * provided only with a limited warranty and the software's author, the
 * holder of the economic rights, and the successive licensors have
 * only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks
 * associated with loading, using, modifying and/or developing or
 * reproducing the software by the user in light of its specific
 * status of free software, that may mean that it is complicated to
 * manipulate, and that also therefore means that it is reserved for
 * developers and experienced professionals having in-depth computer
 * knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions
 * enabling the security of their systems and/or data to be ensured
 * and, more generally, to use and operate it in the same conditions
 * as regards security.

 * The fact that you are presently reading this means that you have
 * had knowledge of the CeCILL license and that you accept its terms.
 *
 **/

#include "CImg.h"
using namespace cimg_library;

#include <matirf>

int main(const int argc, char* argv[]) {
  
  if (argc > 1) {
    if (!cimg::strcasecmp(argv[1],"metadata")) return extract_metadata(argc, argv);
    else if (!cimg::strcasecmp(argv[1],"sequence")) return reconstruct_sequence(argc, argv);
    else if (!cimg::strcasecmp(argv[1],"localize")) return localize(argc, argv);
    else if (!cimg::strcasecmp(argv[1],"model")) return model(argc, argv);
    else if (!cimg::strcasecmp(argv[1],"generate")) return generate_metadata(argc, argv);
    else if (!cimg::strcasecmp(argv[1],"simulate")) return simulate(argc, argv);
    else if (!cimg::strcasecmp(argv[1],"resolution")) return analyze_resolution(argc, argv);
    else { return reconstruct(argc, argv); }
  } else {
    printf("\n  matirf : Multi-Angle TIRF image analysis\n\n"
	   "    metadata    : extract metadata\n"
	   "    generate    : generate metadata\n"
	   "    simulate    : simulate a matirf image stack\n"
	   "    model       : analyze the matirf model\n"
	   "    resolution  : analyze the resolution using a bead\n"
	   "    localize    : localize beads in depth\n"
	   "    reconstruct : reconstruct an matirf image\n"
	   "    sequence    : reconstruct an matirf image sequence\n\n");
  }
}
