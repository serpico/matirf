#!/bin/sh

#build
cd $(dirname "$0")
mkdir build
cd build
cmake ..
make
cd ..

#install
dir=$(pwd)
cp $dir/build/bin/matirf /usr/local/bin/matirf