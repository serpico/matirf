/**
 *
 * \file matirf.cpp
 *
 * \description Main multi-angle TIRF image reconstruction
 *
 * \section licence_sec Licence
 *
 * Copyright Jérôme Boulanger (2013)
 *
 * jerome.boulanger@curie.fr
 *
 * This software is a computer program whose purpose is to reconstruct
 * images acquired in multi-angle total internal reflection microscopy.
 *
 * This software is governed by the CeCILLlicense under French law and
 * abiding by the rules of distribution of free software.  You can use,
 * modify and/ or redistribute the software under the terms of the
 * CeCILL license as circulated by CEA, CNRS and INRIA at the following
 * URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to
 * copy, modify and redistribute granted by the license, users are
 * provided only with a limited warranty and the software's author, the
 * holder of the economic rights, and the successive licensors have
 * only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks
 * associated with loading, using, modifying and/or developing or
 * reproducing the software by the user in light of its specific
 * status of free software, that may mean that it is complicated to
 * manipulate, and that also therefore means that it is reserved for
 * developers and experienced professionals having in-depth computer
 * knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions
 * enabling the security of their systems and/or data to be ensured
 * and, more generally, to use and operate it in the same conditions
 * as regards security.

 * The fact that you are presently reading this means that you have
 * had knowledge of the CeCILL license and that you accept its terms.
 *
 **/

#include "matirf.h"
#include "cimgutils.h"
#include "CFigure.h"

matirf_parameter load_acquisition_parameters(const char * file_i,
					     const char * file_p,
					     const int mode) {
  matirf_parameter param;
  if (file_p) {
    if (mode > 0) printf("** Loading parameters from file '%s'\n", file_p);
    param = load_acquisition_parameters_from_json(file_p);
  } else {
    if (file_i) {
      if (mode > 0) printf("** Loading parameters from file '%s'\n", file_i);
      param = load_acquisition_parameters_from_tiff_metadata(file_i);
    } else {
      printf("input needed (option -i).\n");
    }
  }
  return param;
}

CImg<> load_multi_angle_stack(const char * file_i, const int mode) {
  CImg<> img;
  if (file_i) {
    if (mode > 0) printf("** Loading multi-angle stack from file '%s'\n", file_i);
    img.load(file_i);
  } else {
    printf("input needed (option -i).\n");
  }
  return img;
}


void print_algo_parameters(const char * str,
			   const int nz, const float depth,
			   const float lambda_xy, const float lambda_z,
			   const float gamma, const int max_iter,
			   const int algo, const int regularization,
			   const int block_size, const int mode) {
  if (mode > 0) {
    if (str != NULL) printf("%s\n", str);
    printf("Depth             : %d x %.2fnm (%.2fnm)\n"
	   "Algorithm         : %s\n"
	   "Regularization    : %s (xy: %.3f, z: %.3f)\n"
	   "Iterations        : %d\n"
	   "Gamma             : %.2f\n",
	   nz, depth / (float) nz, depth,
	   get_algorithm_str(algo),
	   get_smoother_str(regularization), lambda_xy, lambda_z,
	   max_iter, gamma);
    if (algo > 1)
      printf("Block size        : %dx%d\n", 2*block_size+1, 2*block_size+1);
  }
}

void load(CImg<> & img, matirf_parameter &param, const char * file_i,
	  const char * file_p, const char * angle_str, const int mode){

  param = load_acquisition_parameters(file_i, file_p, mode);

  img = load_multi_angle_stack(file_i, mode);

  sanity_check(img, param);

  if (mode > 0) printf("** Preprocess matirf image stack\n");
  preprocess_matirf_images(img, param);

  if (angle_str!=0) {
    if (mode > 0) printf("** Subselect angle in range (%s)\n", angle_str);
    select_matirf_angles(img, param, angle_str);
  }

  if (mode > 0) print_matirf_parameters(param);
}

void save(const CImg<> & data, const char * filename, const char * what,
	  const int mode) {
  if (filename) {
    if (mode > 0) printf("** Saving %s to file '%s'\n", what, filename);
    data.save(filename);
  }
}

CImg<> parse_index_parameters(float & scale, const char * index_str,
			      const int mode) {
  int number_of_index = 50;
  float lower_index = 1.33, upper_index = 1.38;
  scale = 4;
  int r = sscanf(index_str,"%f,%f,%d,%f",
		 &lower_index, &upper_index, &number_of_index, &scale);
  if (r==1) {
    upper_index = lower_index;
    if (mode > 0) printf("Using a single index (%.3f)\n", lower_index);
    return CImg<>(1,1,1,1,lower_index);
  }
  if (mode > 0) {
    printf("Scale             : %.2f\n", scale);
    printf("Search range      : %d in [%.3f-%.3f] \n",
	   number_of_index, lower_index, upper_index);
  }
  return CImg<>::sequence(number_of_index, lower_index, upper_index);
}

void parse_regularization_parameters(const char * lambda_str,
				     float & lambda_xy, float & lambda_z) {
  lambda_xy = 0.001;
  lambda_z = 0.01;
  int r = sscanf(lambda_str,"%f,%f", &lambda_xy, &lambda_z);
  if (r != 2) {
    printf("warning: invalid regularization parameters using default (0.001,0.01)\n");
    lambda_xy = 0.001;
    lambda_z = 0.01;
  }
}

//! Localize bead in 3D using model fitting
int localize(const int argc, char * argv[]){
  cimg_usage("Multi-angle TIRF depth localization estimation");
  const char * file_i = cimg_option("-i",(char*)NULL,"input file (ma-tirf image stack)");
  const char * file_p = cimg_option("-p",(char*)NULL,"input file (parameters)");
  const char * angle_str = cimg_option("-angle",(char*)"0:90","angles");

  const char * file_c = cimg_option("-c",(char*)NULL,"input file (coordinates)");
  const char * file_o = cimg_option("-o",(char*)NULL,"ouput file (3D coordinates)");

  const int mode = cimg_option("-mode",(int)1,"mode (0: batch, 1: visu)");
  const float size = cimg_option("-s",(float)40,"objects size (nm)");

  if (mode > 0) printf("Localization\n");

  matirf_parameter param;

  CImg<> img;

  load(img, param, file_i, file_p, angle_str, mode);

  CImg<> coords;

  if (file_c) {
    if (mode > 0) printf("** Loading coordinates from file '%s'\n", file_c);
    coords.load(file_c);
    if (mode > 0) printf("   Number of coordinates loaded : %d\n", coords.width());
  } else {
    if (mode > 0) printf("** Detecting beads\n");
    CImg<> mip = maximum_intensity_projection(img);
    coords = detect_blob(mip,1,1,1e-3);
    coords = meanshift_localization(mip, coords, 1.f) + .5;
    if (mode > 0) printf("   Number of detected beads : %d\n", coords.width());
  }

  if (mode > 0) printf("** Estimating depth\n");
  coords = estimate_depth_from_matirf(coords, img, param, size);

  save(coords.get_transpose(), file_o, "3D coordinates", mode);

  print_localization_result(coords);
  display_localization_result(img, coords);

  return 0;
}

int model(const int argc, char* argv[]) {
  cimg_usage("Multi-angle TIRF model analysis");
  const char * file_i = cimg_option("-i",(char*)NULL,"input file (ma-tirf image stack)");
  const char * file_p = cimg_option("-p",(char*)NULL,"input file (parameters)");

  const char * file_o = cimg_option("-o",(char*)NULL,"ouput file (operator)");
  const char * file_b = cimg_option("-b",(char*)NULL,"ouput file (beads profiles)");

  const float depth = cimg_option("-d",(float)500.f,"depth");
  const float size = cimg_option("-s",(float)40.f,"bead size");
  const int nz = cimg_option("-n",(int)100,"number of planes");
  const bool replace_angle = cimg_option("-a",true,"replace angles");
  const int mode = cimg_option("-mode",(int)1,"mode (0: batch, 1: visu)");

  matirf_parameter param = load_acquisition_parameters(file_i, file_p, mode);
  if (mode > 0) print_matirf_parameters(param);
  if (mode > 0) {
    printf("angle\tdepth\n");
    cimg_foroff(param.angles, i) {
      const float theta = param.angles(i);
      printf("%.2f\t%.2f\n", theta, compute_penetration_depth_nm(theta, param));
    }
  }

  if (replace_angle) param.angles = (CImg<>::sequence(100, 50, 80)).unroll('y');
  param.angles.sort();

  CImg<> H = compute_matirf_operator(param, nz, 0, depth, .01);
  save(H, file_o, "operator matrix", mode);

  CFigure fig(640,480);

  CImg<> U,S,V;
  H.SVD(U,S,V);
  fig.plot(CImg<>::sequence(S.size(), 0, S.size()-1), S.get_max(1e-15).get_log(), "r-").set_axis();
  fig.title("Eigen values of the operator").display();

  CImg<>  depths = CImg<>::sequence(nz + 1, 0, depth);
  depths.unroll('y').rows(0, nz - 1);
  fig.clear();
  CImgList<> profiles(param.angles);
  printf("depth :");
  CImg<> bead_depth=CImg<>::sequence(5,size/2,depth - size/2);
  cimg_foroff(bead_depth, i) {
    float z = bead_depth(i);
    printf("%.0fnm%s", z, i==bead_depth.size()-1?"":",");
    CImg<> f = bead_model(z, size, depths, H);
    profiles.push_back(f.unroll('y')/f(0));
    fig.plot(param.angles, f/f(0), "r");
  }
  printf("\n");
  fig.set_axis();
  fig.title("Beads").display();
  save(profiles.get_append('x'), file_b, "beads profiles (theta,p1,..,pn)", mode);
  return 0;
}

int simulate(const int argc, char* argv[]) {
  cimg_usage("Multi-angle TIRF image simulation");
  const char * file_i = cimg_option("-i",(char*)NULL,"input file (image stack)");
  const char * file_p = cimg_option("-p",(char*)NULL,"input file (parameters)");

  const char * file_o = cimg_option("-o",(char*)NULL,"ouput file (ma-tirf iamge)");
  const float depth = cimg_option("-d",(float)500.f,"depth");

  const int mode = cimg_option("-mode",(int)1,"mode (0: batch, 1: visu)");

  if (!file_i) throw CImgException("Input (option -i) needed.");
  CImg<> img(file_i);
  if (!file_p) throw CImgException("Input (option -p) needed.");
  matirf_parameter param = load_acquisition_parameters_from_json(file_p);
  if (mode > 0) print_matirf_parameters(param);
  CImg<> H = compute_matirf_operator(param, img.depth(), 0, depth);
  CImg<> matirf_img = apply_matirf_operator(H, img);
  save(matirf_img, file_o, "multi-angle stack", mode);
  return 0;
}

//! Generate metadata and save them to a different format
int generate_metadata(const int argc, char* argv[]) {
  cimg_usage("Multi-angle TIRF metadata generation");
  const char * file_o = cimg_option("-o",(char*)NULL,"ouput file (parameters)");
  matirf_parameter param;
  param.wavelength_nm = cimg_option("-w",561,"Wavelength (nm)");
  param.glass_index = cimg_option("-nglass",1.52,"Glass index");
  param.medium_index = cimg_option("-nmedium",1.34,"Medium index");
  param.oil_index = cimg_option("-noil",1.518,"Oil index");
  param.numerical_aperture = cimg_option("-na",1.49,"Numerical Aperture");
  param.beam_divergence = cimg_option("-bdiv",0.3,"Beam divergence");
  const char * angles_str = cimg_option("-angles",(char*)NULL,"Angles (min,max,n)");
  const char * depths_str = cimg_option("-depths",(char*)NULL,"Depths (min,max,n)");
  const int mode = cimg_option("-mode",(int)1,"mode (0: batch, 1: visu)");

  if (angles_str != NULL && depths_str == NULL) { // Sampling in angle
    float theta0 = 61, theta1 = 90;
      int ntheta = 10;
    sscanf(angles_str, "%f,%f,%d", &theta0, &theta1, &ntheta);
    param.angles = CImg<>::sequence(ntheta, theta0, theta1);
  } else if (angles_str == NULL && depths_str != NULL){ // Sampling in penetration depth
    float z0 = 200, z1 = 500;
    int nz = 10;
    sscanf(depths_str, "%f,%f,%d", &z0, &z1, &nz);
    param.angles = CImg<>::sequence(nz, z0, z1);
    cimg_foroff(param.angles, i)
      param.angles(i) = depth_to_angle(param.angles(i), param) / cimg::PI*180.f;
  } else {
    printf("matirf generate: option -angles min,max,n or -depths min,max,n needed\n");
    return 1;
  }
  if (mode > 0) print_matirf_parameters(param);
  if (file_o) {
    if (mode > 0) printf("Saving metadata to file '%s'.\n", file_o);
    save_acquisition_parameters_to_json(param, file_o);
  }
  return 0;
}

//! Extract metadata and save them to a different format
int extract_metadata(const int argc, char* argv[]) {
  cimg_usage("Multi-angle TIRF metadata extraction");
  const char * file_i = cimg_option("-i",(char*)NULL,"input file (image)");
  const char * file_o = cimg_option("-o",(char*)NULL,"ouput file (parameters)");
  const int mode = cimg_option("-mode",(int)1,"mode (0: batch, 1: visu)");
  matirf_parameter param;
  param = load_acquisition_parameters_from_tiff_metadata(file_i);
  if (mode > 0) print_matirf_parameters(param);
  if (file_o) {
    if (mode > 0) printf("Saving metadata to file '%s'.\n", file_o);
    save_acquisition_parameters_to_json(param, file_o);
  }
  return 0;
}

// compute the spread of an intensity profile by computing the 2nd order moments
float profile_spread(const CImg<>&img) {
  float m = 0, s = 0, sw = 0;
  float t = 0.05 * img.max();
  cimg_foroff(img, i) {
    const float w = img(i), x = i;
    if (w > t) {
      m += w * x;
      s += w * x * x;
      sw += w;
    }
  }
  s /= sw;
  m /= sw;
  return std::sqrt(s - m * m);
}

int analyze_resolution(const int argc, char* argv[]) {
  cimg_usage("Multi-angle TIRF resolution analysis");
  const char * file_p = cimg_option("-p",(char*)NULL,"input file (parameters)");
  const float z = cimg_option("-z",(float)250.f,"depth of the bead");
  const float bead_size_nm = cimg_option("-s",(float)10.f,"size of the bead (fwhm)");
  const float depth = cimg_option("-d",(float)500.f,"total depth");
  const float z0 = cimg_option("-zmin",(float)0,"z0");
  const int nz = cimg_option("-n",(int)20,"number of planes");
  const char * lambda_str = cimg_option("-lambda",".001,.0001","regularization (XY,Z)");
  const float gamma = cimg_option("-gamma",(float)10000.f,"gamma / time step");
  const int max_iter = cimg_option("-iter",(int)2000,"number of iterations");
  const int algo = cimg_option("-algo",(int)1,"algorithm");
  const int regularization = cimg_option("-reg",(int)2,"regularization type");
  const float sigma = cimg_option("-noise",(float)0.f,"noise level");
  const int mode = cimg_option("-mode",(int)1,"mode (0: batch, 1: visu)");

  if (z < 0 || z > depth)
    throw CImgException("input parameter -z is out of range (0,%.1f).", depth);

  if (!file_p)
    throw CImgException("input parameter -p needed");

  matirf_parameter param = load_acquisition_parameters_from_json(file_p);
  if (mode > 0) print_matirf_parameters(param);

  // Generate a synthetic sample
  CImg<> img(32, 32, depth, 1, 0);
  const float bead_size = bead_size_nm / depth * (float) img.depth() / 2.35482;
  const float c[1] = {255};
  img.draw_gaussian(img.width()/2.f, img.height()/2.f, z / depth * img.depth(),
		    CImg<>::diagonal(1, 1, bead_size), c);

  // Compute operator for the simulation
  CImg<> H1 = compute_matirf_operator(param, img.depth(), z0, depth);

  // Compute the multi-angle tiff image stack normalize ti and add noise
  CImg<> g = apply_matirf_operator(H1, img);
  g.normalize(0,255);
  if (sigma > 0) g.noise(sigma);

  // Compute the operator for the reconstruction
  CImg<> H2 = compute_matirf_operator(param, nz, z0, depth);
  float lambda_xy, lambda_z;
  parse_regularization_parameters(lambda_str, lambda_xy, lambda_z);

  // Reconstruct the sample from the measurements
  print_algo_parameters("\n** Reconstruction parameters",
			nz, depth, lambda_xy, lambda_z, gamma,
			max_iter, algo, regularization,
			0, mode);

  CImg<> f = reconstruct_matirf(H2, g, lambda_xy, lambda_z, gamma, max_iter,

				algo, regularization, 0, mode);
  // Estimate the FWHM
  CImg<> tmp0 = zprofile(img,16,16,1), tmp1 = zprofile(f,16,16,1);
  float
    r0 = fwhm(tmp0) / (float)img.depth() * depth,
    r1 = fwhm(tmp1) / (float)f.depth() * depth,
    s0px = profile_spread(tmp0),
    s0nm = s0px / (float)img.depth() * depth,
    s1px = profile_spread(tmp1-tmp1.min()),
    s1nm = s1px / (float) f.depth() * depth,
    s2nm = std::sqrt(s1nm * s1nm - s0nm * s0nm),
    s2px = s2nm / depth * (float) f.depth();
  // Print and display results
  if (mode > 0) {
    printf("\nWide Field FWHM\n  lateral\t: %.2fnm\n  axial  \t: %.2fnm\n\n",
	   0.614  * param.wavelength_nm / param.medium_index,
	   1.4 * param.wavelength_nm / param.medium_index);
    printf("Apparent FWHM of a %.2fnm bead at depth %.2fnm: %.2fnm (px size: %.2fnm)\n",
	   r0, z, r1, depth / nz);
    printf("   \t    std   \t    fwhm\n");
    printf("   \t px \t nm \t px \t nm\n");
    printf("sim\t%.2f\t%.2f\t%.2f\t%.2f\n", s0px, s0nm, 2.35482 * s0px, 2.35482 * s0nm);
    printf("rec\t%.2f\t%.2f\t%.2f\t%.2f\n",
	   s1px, s1nm, 2.35482 * s1px, 2.35482 * s1nm);
    printf("psf\t%.2f\t%.2f\t%.2f\t%.2f\n",
	   s2px, s2nm, 2.35482 * s2px, 2.35482 * s2nm);
    img
      .get_normalize(0,1)
      .get_append(f.get_normalize(0,1).get_resize(img, 5),'c')
      .display("reconstructed bead",false);
    tmp0
      .get_normalize(0,1)
      .get_append(tmp1.get_normalize(0,1).get_resize(tmp0, 5),'c')
      .display_graph();
  } else {
    // angles, bead depth, fwhm sim, fwhm rec, fwhm psf
    printf("%d\t%.2f\t%.2f\t%.2f\t%.2f\n", (int) param.angles.size(),
	   z, 2.35482 * s0nm, 2.35482 * s1nm, 2.35482 * s2nm);
  }
  return 0;
}

int reconstruct(const int argc, char* argv[]) {
  cimg_usage("Multi-angle TIRF reconstruction");
  const char * file_i = cimg_option("-i",(char*)NULL,"input file (image)");
  const char * file_p = cimg_option("-p",(char*)NULL,"input file (parameters)");
  const char * file_o = cimg_option("-o",(char*)NULL,"output file (image)");
  const char * file_oh = cimg_option("-oh",(char*)NULL,"output file (operator)");
  const char * file_idx = cimg_option("-idx",(char*)NULL,"output file (index image)");
  const char * angle_str = cimg_option("-angle",(char*)"0:90","angles");
  const char * index_str = cimg_option("-index",(char*)"1.33,1.34,10,4",
				       "index estimation (low,up,n,scale)");
  const float depth = cimg_option("-d",(float)500.f,"depth");
  const int nz = cimg_option("-n",(int)10,"number of planes");
  const float z0 = cimg_option("-zmin",(float)0,"z0");
  const char * lambda_str = cimg_option("-lambda",".001,.001","regularization (XY,Z)");
  const float gamma = cimg_option("-gamma",(float)10000.f,"gamma / time step");
  const int max_iter = cimg_option("-iter",(int)1000,"number of iterations");
  const int algo = cimg_option("-algo",(int)1,"algorithm");
  const int regularization = cimg_option("-reg",(int)1,"regularization type");
  const int block_size = cimg_option("-block",(int)20,"bloc size for blind estimation");
  const int mode = cimg_option("-mode",(int)1,"mode (0:batch, 1:print, 2:visu)");

  matirf_parameter param;
  CImg<> g, f;
  load(g, param, file_i, file_p, angle_str, mode);

  float lambda_xy, lambda_z;
  parse_regularization_parameters(lambda_str, lambda_xy, lambda_z);

  print_algo_parameters("** Reconstruction (multi index, single frame)",
			nz, depth, lambda_xy, lambda_z, gamma,
			max_iter, algo, regularization,
			0, mode);

  float scale = 4;
  CImg<> medium_indexes = parse_index_parameters(scale, index_str, mode);
  CImgList<> H = compute_multi_index_operators(param, nz, z0, depth, medium_indexes);
  CImg<> map(g.width(), g.height(), 1, 1, 0);

  // Index estimation
  if (medium_indexes.size() > 1) {  // Multi index reconstruction
    if (mode > 0) printf("** Estimating refractive index\n");
    const unsigned int tich = cimg::time();
    map = estimate_multi_index_map(H, g,
				   lambda_xy, lambda_z, gamma, max_iter,
				   algo, regularization, scale, mode);
    const unsigned int toch = cimg::time();
    if (mode > 0 || file_idx) {
      CImg<> idx = map.get_map(medium_indexes);
      if (mode > 0) {
	printf("elapsed time         : %.2fs\n", (float)(toch - tich)/1000);
	printf("Estimated range      : [%.3f/%.3f/%.3f]\n",
	       idx.min(),idx.mean(), idx.max());
      }
      save(idx, file_idx, "index map", mode);
      if (mode > 1) idx.display("estimated local indexes", false);
    }
  }

  const unsigned int tic = cimg::time();
  f = reconstruct_matirf(H, map, g,
			 lambda_xy, lambda_z, gamma, max_iter,
			 algo, regularization, block_size, mode);
  const unsigned int toc = cimg::time();

  if (mode > 0)  {
    printf("Elapsed time      : %.2fs\n", (float)(toc - tic)/1000);
    printf("Mean Squared Error: %f\n",
	   compute_multi_index_reconstruction_error_norm(H, map, f, g));
  }

  save(H[0], file_oh, "operator", mode);

  if (file_o) {
    if (mode > 0) printf("Saving reconstruction to file '%s'.\n", file_o);
    f.save(file_o);
  }

  if (mode > 1) f.display("reconstructed", false);

  return 0;
}

//! Load an CImgList<> from a list of files
CImgList<> load_sequence(const char * filename, const int first_frame, const int last_frame) {
  CImgList<> dest;
  for (int frame = first_frame; frame <= last_frame; ++frame) {
    char buf[1024];
    sprintf(buf, filename, frame);
    dest.push_back(CImg<>(buf));
  }
  return dest;
}

//! Save an CImgList<> to a list of files
void save_sequence(const CImgList<> & img, const char * filename,
		   const int first_frame, const int last_frame) {
  for (int frame = first_frame; frame <= last_frame; ++frame) {
    char buf[1024];
    sprintf(buf, filename, frame);
    img[frame - first_frame].save(buf);
  }
}

int reconstruct_sequence(const int argc, char* argv[]) {
  cimg_usage("Multi-angle multi-frame TIRF reconstruction");
  const char * file_i = cimg_option("-i",(char*)NULL,"input file (image)");
  const char * frame_str = cimg_option("-frame",(char*)NULL,"frames (start:end)");
  const int chunk_size = cimg_option("-chunk",20,"chunk size");
  const char * file_p = cimg_option("-p",(char*)NULL,"input file (parameters)");
  const char * file_o = cimg_option("-o",(char*)NULL,"output file (image)");
  const char * file_idx = cimg_option("-idx",(char*)NULL,"output file (index image)");
  const char * index_str = cimg_option("-index",(char*)"1.33,1.34,10,4",
				       "index estimation (low,up,n,scale)");
  const float depth = cimg_option("-d",(float)500.f,"depth");
  const int nz = cimg_option("-n",(int)10,"number of planes");
  const float lambda_xy = cimg_option("-lambda_xy",(float).5f,"XY regularization");
  const float lambda_z = cimg_option("-lambda_z",(float).1f,"Z regularization");
  const float gamma = cimg_option("-gamma",(float)100.f,"time step");
  const int max_iter = cimg_option("-iter",(int)20,"number of iterations");
  const int mode = cimg_option("-mode",(int)1,"mode (0: batch, 1: visu)");

  int first_frame = 0, last_frame = 0;
  if (frame_str) sscanf(frame_str, "%d:%d", &first_frame, &last_frame);

  float index_scale = 4;
  CImg<> medium_indexes = parse_index_parameters(index_scale, index_str, mode);

  for (int chunk_start = first_frame; chunk_start <= last_frame; chunk_start += chunk_size) {
    const int chunk_end = cimg::min(chunk_start + chunk_size - 1, last_frame);
    if (mode > 0) printf("** Loading data from file '%s' (%d:%d)\n",
			 file_i, chunk_start, chunk_end);
    CImgList<> g = load_sequence(file_i, chunk_start, chunk_end);

    matirf_parameter param;
    if (file_p) {
      if (mode > 0) printf("** Loading metadata from file '%s'\n", file_p);
      param = load_acquisition_parameters_from_json(file_p);
    } else {
      printf("input file (option -p) needed.\n");
    }

    if (mode > 0) print_matirf_parameters(param);

    cimglist_for(g, l) sanity_check(g[l], param);

    cimglist_for(g, l) preprocess_matirf_images(g[l], param, l==g.width()-1?true:false);

    // Estimate locally the medium index
    if (mode > 0) {  printf("** Estimating refractive index\n"); cimg::tic();}

    CImgList<> H = compute_multi_index_operators(param, nz, 0, depth, medium_indexes);

    CImgList<int> map = estimate_multi_index_map_multi_frame(H, g, lambda_xy, lambda_z,
							     gamma, max_iter, 1, 1,
							     index_scale, mode);

    if (mode > 0) cimg::toc();
    CImgList<> idx(map.width());
    cimglist_for(map, l) idx[l] = map[l].get_map(medium_indexes);
    if (file_idx) {
      if (mode > 0) printf("Saving index map to file '%s' (%d:%d).\n",
			   file_idx, chunk_start, chunk_end);
      save_sequence(idx, file_idx, chunk_start, chunk_end);
    }

    if (mode > 0) {
      CImg<> idx3d = idx.get_append('z');
      printf("Estimated range   : [%.3f/%.3f/%.3f]\n",
	     idx3d.min(),idx3d.mean(), idx3d.max());
      if (mode > 1) idx3d.display("estimated local indexes", false);
    }

    // Reconstruct with the map of indexes
    if (mode > 0) {
      print_algo_parameters("** Reconstruction (single index, single frame)",
			    nz, depth, lambda_xy, lambda_z, gamma,
			    max_iter, 1, 0,
			    0, mode);
      cimg::tic();
    }

    H = compute_multi_index_operators(param, nz, 0, depth, medium_indexes);
    CImgList<> f = reconstruct_matirf_multi_index_multi_frame_ppxa(H, map, g, gamma, max_iter);

    cimglist_for(f,l) f[l].max(0.f);

    if (mode > 0)   cimg::toc();

    if (file_o) {
      if (mode > 0) printf("Saving reconstruction to file '%s' (%d:%d).\n",
			   file_o, chunk_start, chunk_end);
      save_sequence(f, file_o, chunk_start, chunk_end);
    }

    if (mode > 1) f.display("reconstructed", false);
  }

  return 0;
}
