/**
 *
 * \file parameters.cpp
 *
 * \section licence_sec Licence
 *
 * Copyright Jérôme Boulanger (2013)
 *
 * jerome.boulanger@curie.fr
 *
 * This software is a computer program whose purpose is to reconstruct
 * images acquired in multi-angle total internal reflection microscopy.
 *
 * This software is governed by the CeCILLlicense under French law and
 * abiding by the rules of distribution of free software.  You can use,
 * modify and/ or redistribute the software under the terms of the
 * CeCILL license as circulated by CEA, CNRS and INRIA at the following
 * URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to
 * copy, modify and redistribute granted by the license, users are
 * provided only with a limited warranty and the software's author, the
 * holder of the economic rights, and the successive licensors have
 * only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks
 * associated with loading, using, modifying and/or developing or
 * reproducing the software by the user in light of its specific
 * status of free software, that may mean that it is complicated to
 * manipulate, and that also therefore means that it is reserved for
 * developers and experienced professionals having in-depth computer
 * knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions
 * enabling the security of their systems and/or data to be ensured
 * and, more generally, to use and operate it in the same conditions
 * as regards security.

 * The fact that you are presently reading this means that you have
 * had knowledge of the CeCILL license and that you accept its terms.
 *
 **/
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <boost/algorithm/string.hpp>
#define JSON_SPIRIT_VALUE_ENABLED
#include "../json_spirit/json_spirit.h"
#include "parameters.h"
#include "operator.h"

using namespace json_spirit;
using namespace std;
using namespace cimg_library;

matirf_parameter load_acquisition_parameters_from_json(const char * filename) {
  matirf_parameter param;
  if (!filename) {
    printf("Warning: parameter filename is NULL.\n");
    return param;
  }
  try {
    std::ifstream is(filename);
    Value value;
    read(is, value);
    Object obj = value.get_obj();
    for (Object::const_iterator i = obj.begin(); i != obj.end(); ++i) {
      const std::string& name = i->name_;
      const Value& value = i->value_;
      if (name == "wavelength_nm") param.wavelength_nm = value.get_real();
      if (name == "glass_index") param.glass_index = value.get_real();
      if (name == "medium_index") param.medium_index = value.get_real();
      if (name == "oil_index") param.oil_index = value.get_real();
      if (name == "numerical_aperture") param.numerical_aperture = value.get_real();
      if (name == "beam_divergence") param.beam_divergence = value.get_real();
      if (name == "angles") {
	const Array& array = value.get_array();
	param.angles.resize(1,array.size(),1,1,0);
	for (unsigned int j = 0; j < array.size(); ++j ) {
	  param.angles(j) = array[j].get_real();
	}
      }
    }
  } catch(std::runtime_error e) {
    printf("Error: error while parsing file %s\n", filename);
    printf("       %s\n", e.what());
    throw (std::runtime_error("error while parsing parameter file"));
  }
  return param;
}

void save_acquisition_parameters_to_json(matirf_parameter & param, const char * filename) {
  if (filename != NULL) {
    Object obj;
    obj.push_back(Pair("wavelength_nm", param.wavelength_nm));
    obj.push_back(Pair("glass_index", param.glass_index));
    obj.push_back(Pair("medium_index", param.medium_index));
    obj.push_back(Pair("oil_index", param.oil_index));
    obj.push_back(Pair("numerical_aperture", param.numerical_aperture));
    obj.push_back(Pair("beam_divergence", param.beam_divergence));
    Array array;
    cimg_foroff(param.angles, i) array.push_back(param.angles(i));
    obj.push_back(Pair("angles", array));
    std::ofstream os(filename);
    write(obj, os, pretty_print);
  }
}

matirf_parameter parse_matirf_parameters_from_string(const std::string &txt) {
  matirf_parameter param;
  // Set default parameters not present in acquisition metadata
  param.oil_index = 1.518;
  param.numerical_aperture = 1.49;
  param.beam_divergence = 0.1;
  param.glass_index = 1.515;
  param.medium_index = 1.33;
  param.wavelength_nm = -1;
  std::stringstream f(txt);
  std::string str;
  while (f.good()) {
    f >> str;
    if (str.length() > 0) {
      if (str == "wavelength"){f >> param.wavelength_nm;}
      else if (str == "nglass"){f >> param.glass_index;}
      else if (str == "nmedium"){f >> param.medium_index;}
      else if (str == "ntheta" && param.angles.size() == 0) {
	int n = 0, i = 0;
	f >> n;
	param.angles.assign(1, n);
	while(f.good() && i < n) {
	  f >> param.angles(i);
	  i++;
	}
      }
    }
  }
  return param;
}


matirf_parameter load_acquisition_parameters_from_tiff_metadata(const char * filename) {
  matirf_parameter param;
  if (!filename) {
    printf("Warning: parameter filename is NULL.\n");
    return param;
  }
  TIFFSetWarningHandler(0);
  TIFF * tif = TIFFOpen(filename, "r");
  if (!tif) {
    printf("Warning: cannot open file %s.\n", filename);
    return param;
  }
  char * buf = new char[1024*1024];
  int ret = TIFFGetField(tif,TIFFTAG_IMAGEDESCRIPTION, &buf);
  if (ret != 1) {
    printf("Warning: load comment from TIFF file %s.\n", filename);
    return param;
  }
  std::string txt(buf);
  boost::replace_all(txt, "&amp;#13;&amp;#10;", "\n");
  param = parse_matirf_parameters_from_string(txt);
  return param;
}


void print_matirf_parameters(const matirf_parameter & param) {
  printf("** Acquisition parameters\n");
  printf("Wavelength        : %.0fnm\n", param.wavelength_nm);
  printf("Glass index       : %.2f\n", param.glass_index);
  printf("Medium index      : %.2f\n", param.medium_index);
  printf("Oil index         : %.2f\n", param.oil_index);
  printf("Numerical aperture: %.2f\n", param.numerical_aperture);
  printf("Beam divergence   : %.2f deg\n", param.beam_divergence);
  int c = printf("Angles  (%2d)      : [", param.angles.height());
  cimg_foroff(param.angles, i) {
    if (c > 70) c = printf("\n                     ");
    c += printf("%.2f%s", param.angles(i), i!=param.angles.size()-1?", ":"");
  }
  printf("] deg\n");
  printf("Critical angle    : %.2f\n", compute_tirf_critical_angle(param));
  printf("Max angle         : %.2f\n", compute_tirf_max_angle(param));
}


bool sanity_check(const CImg<> & img, const matirf_parameter & param) {
  bool flagok = true;
  if (img.depth() != param.angles.height()) {
    printf("Warning: Angle list size (%d) is inconsistent with image size (%d).\n",
	   param.angles.height(), img.depth());
    flagok = false;
  }
  if (param.wavelength_nm < 300 || param.wavelength_nm > 600) {
    printf("Warning: Wavelength is incorrect (%.2f).\n", param.wavelength_nm);
    flagok = false;
  }
  if (param.glass_index < 1 || param.glass_index > 2) {
    printf("Warning: Glass index is incorrect (%.2f).\n", param.glass_index);
    flagok = false;
  }
  return flagok;
}
