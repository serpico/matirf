/**
 *
 * \file localize.cpp
 *
 * \section licence_sec Licence
 *
 * Copyright Jérôme Boulanger (2013)
 *
 * jerome.boulanger@curie.fr
 *
 * This software is a computer program whose purpose is to reconstruct
 * images acquired in multi-angle total internal reflection microscopy.
 *
 * This software is governed by the CeCILLlicense under French law and
 * abiding by the rules of distribution of free software.  You can use,
 * modify and/ or redistribute the software under the terms of the
 * CeCILL license as circulated by CEA, CNRS and INRIA at the following
 * URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to
 * copy, modify and redistribute granted by the license, users are
 * provided only with a limited warranty and the software's author, the
 * holder of the economic rights, and the successive licensors have
 * only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks
 * associated with loading, using, modifying and/or developing or
 * reproducing the software by the user in light of its specific
 * status of free software, that may mean that it is complicated to
 * manipulate, and that also therefore means that it is reserved for
 * developers and experienced professionals having in-depth computer
 * knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions
 * enabling the security of their systems and/or data to be ensured
 * and, more generally, to use and operate it in the same conditions
 * as regards security.

 * The fact that you are presently reading this means that you have
 * had knowledge of the CeCILL license and that you accept its terms.
 *
 **/

#include "localize.h"
#include "CImg.h"
#include "cimgutils.h"
#include "operator.h"

using namespace cimg_library;

//! generalized gaussian bead model
/**
   \note the bead profile correspond to a generalized of degree 4 with the given fhwm
**/
CImg<> bead_model(const float z, const float fwhm,
		  const CImg<> &depths, const CImg<> & H) {
  CImg<> dest(1, depths.size());
  cimg_forY(dest, i) {
    float zi = (depths(i) - z) / fwhm;
    dest(i) = exp(- 11 * zi * zi * zi * zi);
  }
  dest = H * dest;
  dest /= dest.mean();
  return dest;
}

CImg<> extract_profile(const CImg<> & data, const float x, const float y) {
  CImg<> profile = zprofile(data, x, y, 1);
  profile /= profile.mean();
  return profile;
}

//! Cost function which is minimized to estimate the detph of the beads
class Cost {
public:
  Cost(const CImg<> & depth, const CImg<> & data, const CImg<> & H, const float fwhm):
    m_depths(depth), m_data(data), m_H(H), m_size(fwhm) {
    m_data.unroll('y');
    if (m_data.size() > 10)
      m_sigma = std::sqrt((m_data - m_data.get_blur(5)).variance(2));

  }
  // \f$ \sum (Yi -f(X_i))^2 \f$
  float operator()(const float z) const {
    return sum_of_squared_residuals(z);
  }

  float sum_of_squared_residuals(const float z) const {
    return (m_data - bead_model(z, m_size, m_depths, m_H)).sqr().sum();
  }

  void show(const float z) const {
      m_data.get_append(bead_model(z, m_size, m_depths, m_H),'c').display_graph();
  }
public:
  CImg<> m_depths, m_data, m_H;
  float m_size, m_sigma;
};

// return x(px),y(px),z(nm),sqrt(SSE),sqrt(noise std),Normalized chi2
CImg<> estimate_depth_from_matirf(const CImg<> & coords,
				  const CImg<> & data,
				  const CImg<> & H,
				  const float fwhm,
				  const CImg<> & depths) {
  CImg<> ncoords(coords.width(), 6);
  const float Z0 = depths.mean(), dz = 100, eps = .1;
  const int max_iter = 20, neval = 5, chi2df = data.depth() - 2;
  const float mchi2 = 1.0 - 2.0 / (9.0 * chi2df),
    schi2 = std::sqrt(2.0 / (9.0 * chi2df));
  //#pragma omp parallel for
  cimg_forX(coords, i) {
    const float x = coords(i,0), y = coords(i,1);
    CImg<> Y = extract_profile(data, x, y);
    Cost cost(depths, Y, H, fwhm);
    ncoords(i,0) = x;
    ncoords(i,1) = y;
    ncoords(i,2) = fmin_quadratic(cost, Z0, dz, neval, eps, max_iter);
    ncoords(i,3) = cost.sum_of_squared_residuals(ncoords(i,2));
    ncoords(i,4) = (Y - Y.get_blur(20)).variance();
    ncoords(i,5) = (std::pow((ncoords(i,3) / ncoords(i,4)) / chi2df , 1./3.) - mchi2) / schi2;
    cost.show(ncoords(i,2));
  }
  return ncoords;
}

void init_model_for_depth_estimation(CImg<> & H, CImg<> & depths, const matirf_parameter & param, const float z0, const float z1, const float nz) {
  depths = CImg<>::sequence(nz + 1, z0, z1).unroll('y');
  H = compute_matirf_operator(depths, param.angles, param.wavelength_nm,
			      param.glass_index, param.medium_index,
			      param.oil_index, param.numerical_aperture,
			      param.beam_divergence, .01);
  depths.rows(0, depths.size() - 2);
}

// return x(px),y(px),z(nm),sqrt(SSE),sqrt(noise std),Normalized chi2
CImg<> estimate_depth_from_matirf(const CImg<> & coords,
				  const CImg<> & data,
				  const matirf_parameter & param,
				  const float fhwm,
				  const float z0, const float z1, const float nz) {
  CImg<> H, depths;
  init_model_for_depth_estimation(H, depths, param, z0, z1, nz);
  return estimate_depth_from_matirf(coords,data,H,fhwm,depths);
}

void print_localization_result(const CImg<> & coords) {
  cimg_forX(coords,i) {
    printf("%.2f\t%.2f\t%.2f\t%1.3g\t%1.3g\t%f (%f)\n",
	   coords(i,0), coords(i,1), coords(i,2), sqrt(coords(i,3)), sqrt(coords(i,4)),
	   1 - normcdf(coords(i,5)), coords(i,5));
  }
  const int ngoods = coords.width() - (int)(coords.get_row(5).threshold(3).sum());
  printf("#goods : %d/%d (%.2f%%)\n",
	 ngoods, coords.width(), (float)ngoods / (float)coords.width() * 100.0);
  printf("depth  : %.2f:%.2f\n", coords.get_row(2).min(), coords.get_row(2).max());
}

CImg<> compute_all_profiles(const CImg<> & coords,
			    const CImg<> & data,
			    const matirf_parameter & param,
			    const float fwhm,
			    const float z0, const float z1, const float nz) {
  CImg<> H, depths;
  init_model_for_depth_estimation(H, depths, param, z0, z1, nz);
  CImg<> profiles(2 * coords.width() + 1, param.angles.size());
  cimg_forY(param.angles, j) profiles(0,j) = param.angles(j);
  cimg_forX(coords, i) {
    const float x = coords(i,0), y = coords(i,1), z = coords(i,2);
    CImg<> f = extract_profile(data, x, y), g = bead_model(z, fwhm, depths, H);
    cimg_forY(param.angles, j) {
      profiles(2 * i, j) = f(j);
      profiles(2 * i + 1, j) = g(j);
    }
  }
  return profiles;
}

void display_localization_result(const CImg<> & data, const CImg<> & coords) {
  CImg<> visu(maximum_intensity_projection(data));
  visu.normalize(0,255).resize(-400,-400,1,3);
  float c[3] = {255, 255, 255}, c1[3] = {255, 128, 128}, c2[3] = {128, 255, 128};
  cimg_forX(coords, i) {
    const int x = 4 * coords(i, 0), y = 4 * coords(i, 1);
    if (coords(i, 5) < 3) visu.draw_circle(x, y, 8, c2, 1, 0U);
    else visu.draw_circle(x, y, 8, c1, 1, 0U);
    visu.draw_text(x + 2, y + 2, "%.1f", c, 0, 1, 13, coords(i,2));
  }
  visu.display("localization result", false);
}
