/**
 *
 * \file matirf.cpp
 *
 * \description Main multi-angle TIRF image reconstruction
 *
 * \section licence_sec Licence
 *
 * Copyright Jérôme Boulanger (2013)
 *
 * jerome.boulanger@curie.fr
 *
 * This software is a computer program whose purpose is to reconstruct
 * images acquired in multi-angle total internal reflection microscopy.
 *
 * This software is governed by the CeCILLlicense under French law and
 * abiding by the rules of distribution of free software.  You can use,
 * modify and/ or redistribute the software under the terms of the
 * CeCILL license as circulated by CEA, CNRS and INRIA at the following
 * URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to
 * copy, modify and redistribute granted by the license, users are
 * provided only with a limited warranty and the software's author, the
 * holder of the economic rights, and the successive licensors have
 * only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks
 * associated with loading, using, modifying and/or developing or
 * reproducing the software by the user in light of its specific
 * status of free software, that may mean that it is complicated to
 * manipulate, and that also therefore means that it is reserved for
 * developers and experienced professionals having in-depth computer
 * knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions
 * enabling the security of their systems and/or data to be ensured
 * and, more generally, to use and operate it in the same conditions
 * as regards security.

 * The fact that you are presently reading this means that you have
 * had knowledge of the CeCILL license and that you accept its terms.
 *
 **/

#include "CImg.h"
using namespace cimg_library;

#include "operator.h"
#include "reconstruct.h"
#include "parameters.h"
#include "estimate.h"
#include "localize.h"

matirf_parameter load_acquisition_parameters(const char * file_i,
					     const char * file_p,
					     const int mode);

CImg<> load_multi_angle_stack(const char * file_i, const int mode);

void print_algo_parameters(const char * str,
			   const int nz, const float depth,
			   const float lambda_xy, const float lambda_z,
			   const float gamma, const int max_iter,
			   const int algo, const int regularization,
			   const int block_size, const int mode);

void load(CImg<> & img, matirf_parameter &param, const char * file_i,
	  const char * file_p, const char * angle_str, const int mode);

void save(const CImg<> & data, const char * filename, const char * what,
	  const int mode);

CImg<> parse_index_parameters(float & scale, const char * index_str,
			      const int mode);

void parse_regularization_parameters(const char * lambda_str,
				     float & lambda_xy, float & lambda_z);

//! Localize bead in 3D using model fitting
int localize(const int argc, char * argv[]);

int model(const int argc, char* argv[]);

int simulate(const int argc, char* argv[]);

//! Generate metadata and save them to a different format
int generate_metadata(const int argc, char* argv[]);

//! Extract metadata and save them to a different format
int extract_metadata(const int argc, char* argv[]);

// compute the spread of an intensity profile by computing the 2nd order moments
float profile_spread(const CImg<>&img);

int analyze_resolution(const int argc, char* argv[]);

int reconstruct(const int argc, char* argv[]);

CImgList<> load_sequence(const char * filename, const int first_frame, const int last_frame);

void save_sequence(const CImgList<> & img, const char * filename,
		   const int first_frame, const int last_frame);

int reconstruct_sequence(const int argc, char* argv[]);
