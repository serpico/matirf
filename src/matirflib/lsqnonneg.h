/* -*- mode:c++ -*- */

#ifndef _LSQNONNEG_H_
#define _LSQNONNEG_H_

#include "CImg.h"
using namespace cimg_library;
#include <iostream>
using namespace std;
#include "nnls.h"

#define cdebug cout<<__FILE__<<":"<<__LINE__<<" "

  

template<typename T>
CImg<T> zeros(const int m, const int n=1, const int l=1, const int k=1){
  return CImg<>(n,m,l,k,0);
}

inline int rows(const CImg<> & x){return x.height();}

inline int columns(const CImg<> & x){return x.width();}

template<typename T>
void disp(const CImg<T> &data, const char * title=0){
  if(title) std::cout<<title;
  if (data.is_empty()) {
    std::cout<<" is empty"<<endl;
  } else{
    std::cout<<endl;
    cimg_forY(data,y) {
      std::cout<<"  ";
      cimg_forX(data,x)
	std::cout<<data(x,y)<<"\t";
      std::cout<<endl;
    }
  }
}

template<typename T>
CImg<bool> equal(const CImg<T> & src, T value) {
  CImg<bool> dest(src.width(),src.height(),src.depth(),src.spectrum(),false);
  cimg_foroff(dest,i) if (src(i) == value) dest(i)=true; else dest(i)=false;
  return dest;
}

template<typename T>
CImg<bool> greater_than(const CImg<T> & src, T value) {
  CImg<bool> dest(src.width(),src.height(),src.depth(),src.spectrum(),false);
  cimg_foroff(dest,i) if (src(i) > value) dest(i)=true; else dest(i)=false;
  return dest;
}

template<typename T>
CImg<bool> lower_than(const CImg<T> & src, T value) {
  CImg<bool> dest(src.width(),src.height(),src.depth(),src.spectrum(),false);
  cimg_foroff(dest,i) if (src(i) < value) dest(i)=true; else dest(i)=false;
  return dest;
}

template<typename T>
CImg<bool> greater_than_or_equal(const CImg<T> & src, T value) {
  CImg<bool> dest(src.width(),src.height(),src.depth(),src.spectrum(),false);
  cimg_foroff(dest,i) if (src(i) >= value) dest(i)=true; else dest(i)=false;
  return dest;
}

template<typename T>
CImg<bool> lower_than_or_equal(const CImg<T> & src, T value) {
  CImg<bool> dest(src.width(),src.height(),src.depth(),src.spectrum(),false);
  cimg_foroff(dest,i) if (src(i) <= value) dest(i)=true; else dest(i)=false;
  return dest;
}

// return the index of the elements of test which are true
CImg<unsigned int> find(const CImg<bool> & test) {
  CImg<unsigned int> index(test.size());  
  int j = 0;
  cimg_foroff(test, i) if(test(i)) { index(j) = i; j++; }
  if (j > 0) return index.crop(0, j-1);
  else return CImg<unsigned int>();
}

// return the index of the elements equals to value
template<typename T>
CImg<unsigned int> find_equal_to(const CImg<T> &src, const T value, const T tol) {
  CImg<unsigned int> idx(src.size());
  int j = 0;
  cimg_foroff(src,i) if (std::abs(src(i) - value) < tol) {idx(j) = i; j++;}
  if (j > 0) return idx.crop(0, j-1);
  else return CImg<unsigned int>();
}

//! return true if any of test is true
bool any(const CImg<bool> & test) {
  bool res = false;
  cimg_foroff(test,i) if(test(i)) {res = true; break;}
  return res;
}

//! return the number of true 
unsigned int number_of_true(const CImg<bool> &flag) {
  unsigned int n = 0; 
  cimg_foroff(flag, i) if(flag(i) > 0) n++;
  return n;
}

//! keep columns indexed by p from src
template<typename T>
CImg<T> keep_columns(const CImg<T> &src, const CImg<unsigned int> & p) {
  CImg<> dest(p.size(), src.height());
  cimg_forY(src, y) cimg_forX(p, x)
    dest(x,y) = src(p(x),y);
  return dest;
} 

//! keep columns indexed by p from src
template<typename T>
CImg<T> keep_columns(const CImg<T> &src, const CImg<bool> & flag) {
  return keep_columns(src,find(flag));
}

//! set the values of dest indexed by idx equal to the values of src
template<typename T>
void set_stride(CImg<T> & dest, const CImg<unsigned int> &idx, const CImg<T> &src){
  if (!idx.is_empty()){
    if (src.size() != idx.size()) 
      throw CImgException("set_stride: dimension missmatch");
    cimg_foroff(idx,i) dest(idx(i)) = src(i);
  }
}

//! set the values of dest indexed by idx equal to value
template<typename T>
void set_stride(CImg<T> & dest, const CImg<unsigned int> &idx, const T value){
  if (!idx.is_empty())
    cimg_foroff(idx,i) dest(idx(i)) = value;      
}

//! get the values of src from the index idx
template<typename T>
CImg<T> get_stride(const CImg<T> & src, const CImg<unsigned int> &idx){
  CImg<T> dest(idx.size());
  int j = 0;
  cimg_foroff(idx,i) {dest(j) = src(idx(i)); j++;}
  return dest;
}

//! get the values of src where flag is true
template<typename T>
CImg<T> get_stride(const CImg<T> & src, const CImg<bool> & flag){
  if (src.size()!=flag.size()) 
    throw CImgException("get_stride(const CImg<T> &, const CImg<bool> &)"
			" (wrong dimensions %d and %d)", src.size(), flag.size());
  int n = 0;
  cimg_foroff(flag, i) if(flag(i) > 0) n++;
  CImg<T> dest(n);
  int j = 0;
  cimg_foroff(src, i) if (flag(i)) {dest(j) = src(i); j++;}
  return dest;
}

//! set the values of src where flag is true
template<typename T>
void set_stride(CImg<T> & dest, const CImg<bool> &flag, const CImg<T> & src){
  if (src.size() != flag.size() || src.size() != dest.size()) 
    throw CImgException("set_stride(CImg<T> &, const CImg<bool> &, const CImg<T> &): "
			"wrong dimensions %d %d %d", dest.size(), flag.size(), src.size());
  cimg_foroff(flag, i) if (flag(i)) dest(i) = src(i);
}

//! set the values of dest to value where flag is true
template<typename T>
void set_stride(CImg<T> & dest, const CImg<bool> &flag, const T value){
  if (dest.size()!=flag.size()) throw CImgException("set_stride (wrong dimensions)");
  cimg_foroff(flag, i) if (flag(i)) dest(i) = value;
}

//! remove elements at position indicated by idx from src
template <typename T>
CImg<T> remove_stride(const CImg<T> & src, const CImg<unsigned int> & idx){
  CImg<> dest(src.size());
  int k = 0;
  cimg_foroff(src, i){
    bool is_in_idx = false;
    cimg_foroff(idx, j) if (i==idx(j)) { is_in_idx = true; break; }
    if (!is_in_idx){
      dest(k) = src(i);
      k++;
    }
  }
  if (k>0) return dest.crop(0,k-1);
  else return CImg<T>();
}

inline CImg<bool> Or(const CImg<bool> & x, const CImg<bool> & y){
  return x+y;
}

inline CImg<bool> And(const CImg<bool> & x, const CImg<bool> & y){
  return x.get_mul(y);
}

CImg<bool> Not(const CImg<bool> & x){
  CImg<bool> z(x.width(),x.height(),x.depth(),x.spectrum());
  cimg_foroff(z,i) z(i) = !x(i);
  return z;
}

template<typename T>
CImg<T> abs(const CImg<T> & img){return img.get_abs();}

template<typename T>
CImg<T> abs(CImg<T> & img){return img.get_abs();}

template<typename T>
inline T min(const CImg<T> & img){return img.min();}

template<typename T>
inline T max(const CImg<T> & img){return img.max();}

//! insert an element assuming src is sorted
template<typename T>
CImg<T> insert_element(CImg<T> & src, T element){
  CImg<> dest(src.size() + 1);
  unsigned int i = 0;  
  if (src.size() > 0) while (src(i) < element && i < src.size()) { dest(i) = src(i); i++;}
  dest(i) = element;
  if (src.size() > 0) while (i < src.size()){ dest(i+1) = src(i); i++;}
  return dest;
}

// solves c x = d , x > 0
// Lawson and Hanson, Solving Least Squares Problems 1973.
template<typename T>
CImg<T> lsqnonneg0(const CImg<T> &c, const CImg<T> & d, 
		   const int max_iter=10000, const T tol=1e-9){
  if (rows(c) != rows(d))
    throw CImgException("lsqnonneg dimensions missmatch (%dx%d) vs %d.",
			rows(c),columns(c),rows(d));
  // LH0: Initialize th variables
  int n = columns(c);
  // Initial guess is 0s.  
  CImg<T> x = zeros<T>(n, 1);
  //  Initialize P, according to zero pattern of x.
  CImg<unsigned int> p = find(greater_than(x, (T)0.0));
  int iter = 0;
  while(iter < max_iter){
    while(iter < max_iter){
      iter++;
      //cout<<"iter:"<<iter<<endl;
      // LH6: compute the positive matrix and find the min norm solution
      CImg<T> xtmp;
      if (!p.is_empty()) xtmp = d.get_solve(keep_columns(c, p));
      CImg<unsigned int> idx = find(lower_than(xtmp, (T)0));
      if (idx.is_empty()){
	// LH7: found the solution, iterate
	x = 0.f;
	set_stride(x, p, xtmp);
	break;
      } else {
	// LH8, LH9: find the scaling factor.
	CImg<unsigned int> pidx = get_stride(p, idx);
	CImg<T> xpidx = get_stride(x, pidx), xtmpidx = get_stride(xtmp, idx);
	CImg<T> sf = xpidx.div(xpidx - get_stride(xtmp, idx));
	T alpha = sf.min();
	//cout<<"alpha:"<<alpha<<endl;
	// LH10: adjust X.
	CImg<T> xx = zeros<T>(n, 1);
	set_stride(xx, p, xtmp);
	x += alpha * (xx - x);
	// LH11: move from P to Z all X == 0.
	idx = get_stride(idx, find_equal_to(sf, alpha, tol));
	p = remove_stride(p, idx);
      }
    }
    // Compute the gradient
    CImg<T> w = c.get_transpose()*(d - c * x);
    w = remove_stride(w, p).transpose();
    if (!any(greater_than(w, (T)0))) break; // finish
    // find the maximum gradient.
    CImg<unsigned int> idx = find_equal_to(w, w.max(), tol);
    if (idx.size() > 1) { idx = idx(0); idx.crop(0,0);}
    // move the index from Z to P. Keep P sorted.
    CImg<unsigned int> z = CImg<unsigned int>::sequence(n,0,n-1);
    z = remove_stride(z, p).transpose();
    p = insert_element(p, z(idx(0)));
  }
  // LH12: complete
  return x;
}

  
template<typename T>
CImg<T> lsqnonneg1(const CImg<T> &C, const CImg<T> & d, 
		   const int max_iter=10000, const T tol=1e-9){
  const int n = C.width();
  CImg<bool> P(1,n,1,1,false), Z(1,n,1,1,true),Q;
  CImg<T> 
    x(1,n,1,1,0), wz(1,n,1,1,0), z(1,n,1,1,0), w = C.get_transpose() * (d - C * x),
    xq;
  CImg<unsigned int> t;
  int iter = 0;
  while (any(Z) && any(greater_than(get_stride(w, Z), tol))){
    z.fill(0);
    set_stride(wz, P, cimg::type<T>::min());
    set_stride(wz, Z, w);
    t = find_equal_to(wz, max(wz), tol);
    set_stride(P, t, true);
    set_stride(Z, t, false);
    t = find(P);
    set_stride(z, t, d.get_solve(keep_columns(C, t)));
    while (any(lower_than_or_equal(get_stride(z, P), tol))){
      iter++;
      if (iter > max_iter) {return x; }
      Q = And(lower_than_or_equal(z, tol), P);
      xq = get_stride(x, Q);
      const T alpha = min(xq.div(xq - get_stride(z, Q)));
      x += alpha * ( z - x );
      Z = Or(Z, And(lower_than(abs(x), tol), P));
      P = Not(Z);
      z.fill(0);
      t = find(P);
      set_stride(z, t, d.get_solve(keep_columns(C, t)));
    }
    x = z;
    w = C.get_transpose() * (d - C * x);
  }
  return x;
}


//! Non negative least square using the original fortan code (translated in c)
template<typename T>
CImg<T> lsqnonneg2(const CImg<T> &c, const CImg<T> & d){

  int m = rows(c)/*height*/, n = columns(c);/*width*/

  double * a = new double[m*n];
  for (int i = 0; i < n; ++i) {
    for(int j = 0; j < m; ++j) {
      a[i * m + j] = c(i, j);
    }
  }

  double * rhs = new double[m];
  for (int j = 0; j < m; j++){
    rhs[j] = d(j);
  }

  double *x = new double[n];
  double rnorm;
  double *w = new double[n];
  double *zz = new double[m];
  int *indx = new int[3*n];
  int mode;

  nnls(a, m, m, n, rhs, x, &rnorm, w, zz, indx, &mode);

  CImg<T> dest(1,n);
  for (int i = 0; i < n; ++i) dest(i) = (T)x[i];

  delete[] a;
  delete[] rhs;
  delete[] x;
  delete[] w;
  delete[] zz;
  delete[] indx;
  return dest;
}


template<typename T> 
CImg<T> lsqnonneg(const CImg<T> &c, const CImg<T> & d, const int method=0, 
			       const int max_iter=10000, const T tol=1e-9){
  switch (method){
  case 1: return lsqnonneg1(c,d,max_iter,tol); break;
  case 2: return lsqnonneg2(c,d); break;
  default:  return lsqnonneg0(c,d,max_iter,tol); break;    
  }

}
  
#endif /* _LSQNONNEG_H_ */
