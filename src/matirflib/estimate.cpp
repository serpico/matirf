/**
 *
 * \file estimate.cpp
 *
 * \section licence_sec Licence
 *
 * Copyright Jérôme Boulanger (2013)
 *
 * jerome.boulanger@curie.fr
 *
 * This software is a computer program whose purpose is to reconstruct
 * images acquired in multi-angle total internal reflection microscopy.
 *
 * This software is governed by the CeCILLlicense under French law and
 * abiding by the rules of distribution of free software.  You can use,
 * modify and/ or redistribute the software under the terms of the
 * CeCILL license as circulated by CEA, CNRS and INRIA at the following
 * URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to
 * copy, modify and redistribute granted by the license, users are
 * provided only with a limited warranty and the software's author, the
 * holder of the economic rights, and the successive licensors have
 * only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks
 * associated with loading, using, modifying and/or developing or
 * reproducing the software by the user in light of its specific
 * status of free software, that may mean that it is complicated to
 * manipulate, and that also therefore means that it is reserved for
 * developers and experienced professionals having in-depth computer
 * knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions
 * enabling the security of their systems and/or data to be ensured
 * and, more generally, to use and operate it in the same conditions
 * as regards security.

 * The fact that you are presently reading this means that you have
 * had knowledge of the CeCILL license and that you accept its terms.
 *
 */

#include "estimate.h"
#include "operator.h"
#include "reconstruct.h"

CImgList<> compute_multi_index_operators(const matirf_parameter &param,
					 const float nz, const float z0, const float z1,
					 const CImg<> &index_list, const float precision) {
  CImgList<> H;
  CImg<> depths = CImg<>::sequence(nz + 1, z0, z1);
  cimg_foroff(index_list, l) {
    H.push_back(compute_matirf_operator(depths, param.angles, param.wavelength_nm,
					param.glass_index, index_list(l),
					param.oil_index, param.numerical_aperture,
					param.beam_divergence, precision));
  }
  return H;
}

void print_progress(const int current, const int end) {
  const float x = (float) current / (float)(end - 1);
  const int length = 30, fraction = x * length;
  printf("\r[");
  for (int i = 0; i < length; i++)  {
    if (i < fraction) { printf("-"); }
    else {printf(" ");}
  }
  printf("] %3d%%", (int)(x * 100));fflush(stdout);
  if (current == end - 1) {printf("\n");}
}

// estimate the index using an exhaustive search
CImg<int> estimate_multi_index_map(const CImgList<> & H, const CImg<> & data,
				   const float lambda_xy, const float lambda_z,
				   const float gamma, const int max_iter,
				   const int algo, const int regularization,
				   const float scale, const int mode) {
  const float nscale = cimg::max(1.f, 1.f, scale);
  CImg<int> map(data.width(), data.height(), 1, 1, 0);
  CImg<> emin(data.width(), data.height(), 1, 1, 1e6);
  CImg<> sdata(data);
  if (nscale > 1.1f)
    sdata.resize(data.width() / nscale, data.height() / nscale,
		 data.depth(), 1, 1);
  cimglist_for(H, l) {
    if (mode > 0) print_progress(l, H.width());
    if (l%4==0) {
      if (mode > 0) print_progress(l, H.width());
      CImg<>
	f = reconstruct_matirf(H[l], sdata, lambda_xy, lambda_z, gamma,
			       max_iter, algo, regularization, 0).max(0.f),
	residuals = compute_reconstruction_residuals(H[l], f, sdata);
      residuals.sqr().blur(2.f,true,true).sqrt();
      if (nscale > 1.1f)
	residuals.resize(data.width(), data.height(), residuals.depth(), 1, 5);
#pragma omp parallel for collapse(2)
      cimg_forXY(residuals,x,y) {
	float e = 0;
	cimg_forZ(residuals, z) { e += residuals(x,y,z); }
	if (e < emin(x,y)) {
	  emin(x,y) = e;
	  map(x,y) = l;
	}
      }
    }
  }
  map = (CImg<>(map)).blur(2.f,true,true).round();
  return map;
}

CImgList<int> estimate_multi_index_map_multi_frame(const CImgList<> & H, const CImgList<> & data,
						   const float lambda_xy, const float lambda_z,
						   const float gamma, const int max_iter,
						   const int algo, const int regularization,
						   const float scale, const int mode) {
  if (algo != 1)
    throw CImgException("estimate_multi_index_map_multi_frame: only algorithm=1 is implemented.");
  const float nscale = cimg::max(1.f, 1.f, scale);
  CImgList<int> map(data.width(), data[0].width(), data[0].height(), 1, 1, 0);
  CImgList<> emin(data.width(), data[0].width(), data[0].height(), 1, 1, 1e6);
  CImgList<> sdata(data);
  if (nscale > 1.1f)
    cimglist_for(sdata, k)
      sdata[k].resize(sdata[k].width() / nscale, sdata[k].height() / nscale,
		      sdata[k].depth(), 1, 1);
  cimglist_for(H, l) {
    if (mode > 0) print_progress(l, H.width());
    if (l%4==0) {
      if (mode > 0) print_progress(l, H.width());
      CImgList<> f = reconstruct_matirf_multi_frame_ppxa(H[l], sdata,
							 lambda_xy, lambda_z,
							 gamma, max_iter,
							 regularization);
      cimglist_for(f, k) {
	f[k].max(0.f);
	CImg<> residuals = compute_reconstruction_residuals(H[l], f[k], sdata[k]);
	if (nscale > 1.1f)
	  residuals.resize(data[k].width(), data[k].height(), residuals.depth(), 1, 5);
#pragma omp parallel for collapse(2)
	cimg_forXY(residuals,x,y) {
	  float e = 0;
	  cimg_forZ(residuals, z) { e += residuals(x,y,z); }
	  if (e < emin[k](x,y)) {
	    emin[k](x,y) = e;
	    map[k](x,y) = l;
	  }
	}
      }
    }
  }
  map = (CImg<>(map.get_append('z'))).blur(2.f,true,true).round().get_split('z');
  return map;
}

int find_optimal_index(const CImg<int> & values, const CImg<> &data) {
  if (values.size() != data.size())
    throw CImgException("find_optimal_index(const CImg<>&, const CImg<>&):"
			" size missmatch %d / %d", values.size(), data.size());
  CImg<> X(3, values.size()), Y(1, data.size());
  for (int i = 0; i < (int)values.size(); ++i){
    const float x = values(i);
    X(0,i) = 1;
    X(1,i) = x;
    X(2,i) = x * x;
    Y(i)   = data(i);
  }
  CImg<> p = X.get_pseudoinvert() * Y;// p(2)x^2+p(1)x+p(0)
  float xopt;
  if (std::abs(p(2)) > 0) { xopt = - p(1) / (2.f * p(2)); }
  else xopt = 0;
  return (int)cimg::round(xopt);
}

// Approximate the cost function by a quadratic to find the minimum
CImg<int> estimate_multi_index_map2(const CImgList<> & H, const CImg<> & data,
				    const int N,
				    const float gamma, const int max_iter,
				    const float scale, const int mode) {
  const float nscale = cimg::max(1.f, 1.f, scale);

  CImg<> residuals(data.width(), data.height(), N);
  CImg<> sdata(data);
  if (nscale > 1.1f)
    sdata.resize(data.width() / nscale, data.height() / nscale,
		 data.depth(), 1, 1);
  CImg<> subset(N);
  // compute the residuals for a subset of operators
  cimg_forZ(residuals, l) {
    if (mode > 0) print_progress(l, N);
    const int L = l / (float)(N - 1) * (float)(H.width() - 1);
    subset(l) = L;
    CImg<> f = reconstruct_matirf_ppxa(H[L], sdata, gamma, max_iter).max(0.f);
    CImg<> R = compute_reconstruction_residuals(H[L], f, sdata);
    //R.sqr().blur(2.f, true, true).sqrt();
    if (nscale > 1.1f)
      R.resize(data.width(), data.height(), R.depth(), 1, 5);
    cimg_forXYZ(R, x, y, z) { residuals(x,y,l) += R(x,y,z) * R(x,y,z); }
    residuals.get_shared_slice(l).blur(2.f, true, true);
  }
  residuals.deriche(1,0,'z');
  // find the optimal operator index by approximating the cost of each
  // operator by a quadatric function of the index.
  residuals.display("Residuals", false);
  CImg<int> map(data.width(), data.height(), 1, 1, 0);
#pragma omp parallel for collapse(2)
  cimg_forXY(map, x, y) {
    CImg<> vec = residuals.get_crop(x,y,0,x,y,residuals.depth()-1);
    map(x,y) = cimg::min(H.width()-1,cimg::max(0, find_optimal_index(subset, vec)));
  }
  map = (CImg<>(map)).blur(2.f,true,true).round();
  return map;
}

// Cost function evaluation ||hf-g|| for f=argmin ||hf-g|| + R
CImg<> eval(const CImgList<> & H, const CImg<int> & map, const CImg<> & data,
	    const float gamma, const int max_iter, const float scale) {
  const float nscale = cimg::max(1.f, 1.f, scale);
  CImg<> sdata(data);
  if (nscale > 1.1f)
    sdata.resize(data.width() / nscale, data.height() / nscale, data.depth(), 1, 1);
  CImg<> f = reconstruct_matirf_multi_index_ppxa(H, map, sdata, gamma, max_iter).max(0.f);
  CImg<> R = compute_multi_index_reconstruction_residuals(H, map, f, sdata);
  //R.sqr().blur(2.f, true, true);
  if (nscale > 1.1f)
    R.resize(data.width(), data.height(), R.depth(), 1, 5);
  CImg<> residuals(data.width(), data.height(),1,1,0);
  cimg_forXYZ(R, x, y, z) { residuals(x,y) += R(x,y,z) * R(x,y,z); }
  residuals /= (float)R.depth();
  residuals.sqrt();
  return residuals;
}

CImg<> forward_diff(const CImgList<> & H, const CImg<> & map, const CImg<> & data,
		    const float gamma, const int max_iter, const float scale) {
  CImg<> X0(map.width(),map.height()), X1(map.width(),map.height());
  cimg_foroff(map,i) {
    if (map(i) < H.width() - 2) {X0(i) = map(i); X1(i) = map(i) + 1;}
    else {X0(i) = map(i) - 1; X1(i) = map(i);}
  }
  CImg<> df = eval(H,X1,data,gamma,max_iter,scale) - eval(H,X0,data,gamma,max_iter,scale);
  return df.div(X1 - X0);
}

// Gradient descent with numerical derivatives
CImg<int>  estimate_multi_index_map3(const CImgList<> & H, const CImg<> & data,
				     const int N,
				     const float gamma, const int max_iter,
				     const float scale, const int mode) {
  CImg<> map(data.width(), data.height(), 1, 1,0);
  CImgDisplay disp;
  for (int n = 0; n < N; ++n) {
    if (mode > 0) print_progress(n, N);
    CImg<> dmap = forward_diff(H, map, data, gamma, max_iter, scale);
    //printf("(%f,%f)(%f,%f)\n", map.min(), map.max(), dmap.min(), dmap.max());
    float alpha = 10, cmin=1e12;
    for (float a = .1; a < 2; a *= 2) {
      CImg<int> tmp = (map-a*dmap).cut(0, (int)H.width() - 2).round();
      CImg<> f = reconstruct_matirf_multi_index_ppxa(H, tmp, data, gamma, max_iter).max(0.f);
      const float val = compute_multi_index_reconstruction_error_norm(H, tmp, f, data);
      if (val < cmin) {
	alpha = a; cmin = val;
      }
    }
    printf("a: %f e:%f\n", alpha, alpha*cimg::max(std::abs(dmap.min()),std::abs(dmap.max())));
    map -= alpha * dmap;
    //map.blur(2.f);
    map.cut(0, (int)H.width() - 2);
    map.round();
    if(mode > 0) disp.display(map);
  }
  return map;
}

CImg<int>  estimate_multi_index_map_gen(const CImgList<> & H, const CImg<> & data,
					const float gamma, const int max_iter,
					const float scale, const int mode, const int idxmode) {
  CImg<int> map;
  switch(idxmode) {
  case 2:
    map = estimate_multi_index_map2(H, data, 10, gamma, max_iter, scale, mode);
    break;
  case 3:
    map = estimate_multi_index_map3(H, data, 30, gamma, max_iter, scale, mode);
    break;
  default:
    map = estimate_multi_index_map(H, data, gamma, max_iter, scale, mode);
  }
  return map;
}
