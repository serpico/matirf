// -*- mode: c++; -*-
/**
 *
 * \file estimate.h
 *
 * \section licence_sec Licence
 *
 * Copyright Jérôme Boulanger (2013)
 *
 * jerome.boulanger@curie.fr
 *
 * This software is a computer program whose purpose is to reconstruct
 * images acquired in multi-angle total internal reflection microscopy.
 *
 * This software is governed by the CeCILLlicense under French law and
 * abiding by the rules of distribution of free software.  You can use,
 * modify and/ or redistribute the software under the terms of the
 * CeCILL license as circulated by CEA, CNRS and INRIA at the following
 * URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to
 * copy, modify and redistribute granted by the license, users are
 * provided only with a limited warranty and the software's author, the
 * holder of the economic rights, and the successive licensors have
 * only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks
 * associated with loading, using, modifying and/or developing or
 * reproducing the software by the user in light of its specific
 * status of free software, that may mean that it is complicated to
 * manipulate, and that also therefore means that it is reserved for
 * developers and experienced professionals having in-depth computer
 * knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions
 * enabling the security of their systems and/or data to be ensured
 * and, more generally, to use and operate it in the same conditions
 * as regards security.

 * The fact that you are presently reading this means that you have
 * had knowledge of the CeCILL license and that you accept its terms.
 *
 **/
#pragma once

#include "CImg.h"
#include "parameters.h"
#include "reconstruct.h"

using namespace cimg_library;

CImgList<> compute_multi_index_operators(const matirf_parameter &param,
					 const float nz, const float z0, const float z1,
					 const CImg<> &index_list, const float precision=.1);

//! Point-wise estimate the best operator in a collection minimizing the error
/**
   Exhaustive search
**/
CImg<int> estimate_multi_index_map(const CImgList<> & H, const CImg<> & data,
				   const float lambda_xy, const float lambda_z,
				   const float gamma, const int max_iter,
				   const int algo = 1, const int regularization = 1,
				   const float scale = 4, const int mode = 0);

//! Point-wise estimate the best operator in a collection minimizing the error
/**
   \param algo algorithm (only algorithm is implemented for now)
   Exhaustive search
   Multi-frame version

**/
CImgList<int> estimate_multi_index_map_multi_frame(const CImgList<> & H, const CImgList<> & data,
						   const float lambda_xy, const float lambda_z,
						   const float gamma, const int max_iter,
						   const int algo = 1, const int regularization = 1,
						   const float scale = 4, const int mode = 0);

//! Point-wise estimate the best operator in a collection minimizing the error
/**
   Quadratic approximation
**/
CImg<int> estimate_multi_index_map2(const CImgList<> & H, const CImg<> & data,
				    const int N,
				    const float gamma, const int max_iter,
				    const float scale, const int mode);

//! Point-wise estimate the best operator in a collection minimizing the error
/**
   Gradient descent
**/
CImg<int>  estimate_multi_index_map3(const CImgList<> & H, const CImg<> & data,
				     const int N,
				     const float gamma, const int max_iter,
				     const float scale, const int mode);


CImg<int>  estimate_multi_index_map_gen(const CImgList<> & H, const CImg<> & data,
					const float gamma, const int max_iter,
					const float scale, const int mode=1, const int idxmode=1);
