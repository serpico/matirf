// -*- mode: c++; -*-
/**
 *
 * \file operator.cpp
 *
 * \section licence_sec Licence
 *
 * Copyright Jérôme Boulanger (2013)
 *
 * jerome.boulanger@curie.fr
 *
 * This software is a computer program whose purpose is to reconstruct
 * images acquired in multi-angle total internal reflection microscopy.
 *
 * This software is governed by the CeCILLlicense under French law and
 * abiding by the rules of distribution of free software.  You can use,
 * modify and/ or redistribute the software under the terms of the
 * CeCILL license as circulated by CEA, CNRS and INRIA at the following
 * URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to
 * copy, modify and redistribute granted by the license, users are
 * provided only with a limited warranty and the software's author, the
 * holder of the economic rights, and the successive licensors have
 * only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks
 * associated with loading, using, modifying and/or developing or
 * reproducing the software by the user in light of its specific
 * status of free software, that may mean that it is complicated to
 * manipulate, and that also therefore means that it is reserved for
 * developers and experienced professionals having in-depth computer
 * knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions
 * enabling the security of their systems and/or data to be ensured
 * and, more generally, to use and operate it in the same conditions
 * as regards security.

 * The fact that you are presently reading this means that you have
 * had knowledge of the CeCILL license and that you accept its terms.
 *
 **/

#include "CImg.h"
using namespace cimg_library;

#include "parameters.h"
#include <complex>

// return the TIRF critical angle in degree
float compute_tirf_critical_angle(const float glass_index, const float medium_index) {
  return std::asin(medium_index / glass_index) / cimg::PI * 180.0f;
}

float compute_tirf_critical_angle(const matirf_parameter &param) {
  return compute_tirf_critical_angle(param.glass_index, param.medium_index);
}

float depth_to_angle(const float depth_nm, const float wavelength_nm,
		     const float glass_index, const float medium_index) {
  const float lambda_over_4pid =  wavelength_nm / (4 * cimg::PI * depth_nm);
  return std::asin(std::sqrt((lambda_over_4pid * lambda_over_4pid + medium_index * medium_index) / ( glass_index * glass_index )));
}

float depth_to_angle(const float depth_nm, const matirf_parameter & param) {
  return depth_to_angle(depth_nm, param.wavelength_nm, param.glass_index, param.medium_index);
}

// return the max TIRF angle in degree
float compute_tirf_max_angle(const float oil_index, const float numerical_aperture) {
  return std::asin(numerical_aperture / oil_index) / cimg::PI * 180.0f - 0.5f;
}

float compute_tirf_max_angle(const matirf_parameter &param) {
  return compute_tirf_max_angle(param.oil_index, param.numerical_aperture);
}

// return the TIRF damping coefficient in nm^-1
float compute_tirf_damping(const float angle_in_degree, const float wavelength_nm,
			   const float glass_index, const float medium_index) {
  if (angle_in_degree > compute_tirf_critical_angle(glass_index, medium_index)) {
    const float
      theta = cimg::min(89.0f , angle_in_degree) / 180.0f * cimg::PI,
      A =  glass_index * std::sin(theta), B = medium_index;
    return 4.0f * cimg::PI * std::sqrt( A * A - B * B ) / wavelength_nm;
  } else {
    return 0.0f;
  }
}

float compute_penetration_depth_nm(const float angle_in_degree, const float wavelength_nm,
				   const float glass_index, const float medium_index) {
  return 1.0f / compute_tirf_damping(angle_in_degree, wavelength_nm,
				     glass_index, medium_index);
}

float compute_penetration_depth_nm(const float angle_in_degree,
				   const matirf_parameter & param) {
  return 1.0f / compute_tirf_damping(angle_in_degree, param.wavelength_nm,
				     param.glass_index, param.medium_index);
}

// compute the intensity at the interface using fresnel transmission coefficients
float compute_tirf_intensity_at_interface(const float angle_in_degree,
					  const float glass_index, const float medium_index) {
  const  std::complex<float> n = medium_index / glass_index,
    theta = angle_in_degree / 180.0f * cimg::PI,
    c1 = std::cos(theta), c2 =  std::sqrt(n * n - std::sin(theta) * std::sin(theta)),
    ts = 2.0f * c1 / ( c1 + c2),
    tp = 2.0f * n * c1 / ( n * n * c1 + c2),
    Ip = std::conj(tp) * tp,
    Is = std::conj(ts) * ts;
  return 0.75f * std::abs(Is) + 0.25f * std::abs(Ip);
}

// compute the intensity contribution for a given angle and depth
inline float compute_tirf_contribution(const float theta, const float depth, const float kappa,
				       const float I0, const float theta0, const float theta1) {
  if (theta < theta0) return I0;
  if (theta < theta1) return I0 * exp( - kappa * depth );
  return 0.0f;
}

// normalize the operator by its highest eigen value
CImg<> normalize_operator(const CImg<> & A) {
    CImg<> U,S,V;
    A.SVD(U,S,V);
    return A / S.max();
}

// return the TIRF operator matrix from physical parameters
CImg<> compute_matirf_operator(const CImg<> & depths, const CImg<> & angles,
			       const float wavelength_nm, const float glass_index,
			       const float medium_index, const float oil_index,
			       const float numerical_aperture, const float beam_divergence_in_degree,
			       const float precision) {
  CImg<> H(depths.size() - 1, angles.size(),1,1,0), sw(H);
  const float
    theta0 = compute_tirf_critical_angle(glass_index, medium_index),
    theta1 = compute_tirf_max_angle(oil_index, numerical_aperture),
    nprecision = cimg::min(1.f, cimg::max(1e-5f, precision));
#pragma omp parallel for
  cimg_forY(H,j) {
    const float
      theta = cimg::min(89.0f, cimg::max(0, angles(j))),
      s = beam_divergence_in_degree / std::cos(theta / 180.0f * cimg::PI),
      a = cimg::min(89.f, cimg::max(0, theta - 3.0f * s)),
      b = cimg::min(89.f, cimg::max(0, theta + 3.0f * s));
    for (float alpha = a; alpha < b; alpha += nprecision * (b - a)) {
      const float
	dalpha = (alpha - theta) / s,
	weight = std::exp( - 0.5f * dalpha * dalpha ),
	I0 = compute_tirf_intensity_at_interface(alpha, glass_index, medium_index),
	kappa = compute_tirf_damping(alpha, wavelength_nm, glass_index, medium_index);
      cimg_forX(H, i) {
	const float z0 = depths(i), z1 = depths(i + 1);
	for (float z = z0; z < z1; z += nprecision * (z1 - z0)) {
	  const float pw = compute_tirf_contribution(alpha, z, kappa, I0, theta0, theta1);
	  H(i,j) += weight * pw;
	  sw(i,j) += weight;
	}
      }
    }
  }
  cimg_foroff(H, off) {
    if (sw(off) > 1e-12) H(off) /= sw(off);
    else H(off) = 0;
  }
  return normalize_operator(H);
}

CImg<> compute_matirf_operator(const matirf_parameter & param,
			       const int nz, const float z0, const float z1,
			       const float precision) {
  // compute depths such that nz=3 z1=300 produces: [0-100][100-200][100-300]
  CImg<> depths = CImg<>::sequence(nz + 1, z0, z1);
  return compute_matirf_operator(depths, param.angles, param.wavelength_nm, param.glass_index, param.medium_index,
				 param.oil_index, param.numerical_aperture, param.beam_divergence, precision);
}

CImg<> compute_matirf_operator_jacobian(const CImg<> & depths, const CImg<> & angles,
					const float wavelength_nm, const float glass_index,
					const float medium_index, const float oil_index,
					const float numerical_aperture, const float beam_divergence_in_degree,
					const float precision) {
  CImg<> H(depths.size() - 1, angles.size(),1,1,0), sw(H);
  const float
    theta0 = compute_tirf_critical_angle(glass_index, medium_index),
    theta1 = compute_tirf_max_angle(oil_index, numerical_aperture),
    nprecision = cimg::min(1.f, cimg::max(1e-5f, precision));
#pragma omp parallel for
  cimg_forY(H,j) {
    const float
      theta = cimg::min(89.0f, cimg::max(0, angles(j))),
      s = beam_divergence_in_degree / std::cos(theta / 180.0f * cimg::PI),
      a = cimg::min(89.f, cimg::max(0, theta - 3.0f * s)),
      b = cimg::min(89.f, cimg::max(0, theta + 3.0f * s));
    for (float alpha = a; alpha < b; alpha += nprecision * (b - a)) {
      const float
	dalpha = (alpha - theta) / s,
	weight = std::exp( - 0.5f * dalpha * dalpha ),
	I0 = compute_tirf_intensity_at_interface(alpha, glass_index, medium_index),
	kappa = compute_tirf_damping(alpha, wavelength_nm, glass_index, medium_index);
      cimg_forX(H, i) {
	const float z0 = depths(i), z1 = depths(i + 1);
	for (float z = z0; z < z1; z += nprecision * (z1 - z0)) {
	  const float pw = compute_tirf_contribution(alpha, z, kappa, I0, theta0, theta1);
	  const float e = -medium_index * z * std::pow(std::pow(medium_index*std::sin(alpha / 180.f * cimg::PI),2) - glass_index*glass_index, - 3.f / 2.f);
	  H(i,j) += weight * e * pw;
	  sw(i,j) += weight;
	}
      }
    }
  }
  cimg_foroff(H, off) {
    if (sw(off) > 1e-12) H(off) /= sw(off);
    else H(off) = 0;
  }
  return H;
}

CImg<> apply_matirf_operator(const CImg<> & h, const CImg<> & src) {
  CImg<> dest(src.width(), src.height(), h.height(), 1, 0);
#if 0
#pragma omp parallel for schedule(dynamic)
  cimg_forXY(src,x,y) {
    CImg<> f = h * src.get_crop(x,y,0,x,y,src.depth()-1).unroll('y');
    cimg_forZ(dest, z) dest(x,y,z) = f(z);
  }
#else
#pragma omp parallel for schedule(dynamic)
  cimg_forXY(src,x,y) {
    cimg_forZ(dest, theta) cimg_forZ(src, z)
      dest(x,y,theta) += h(z,theta) * src(x,y,z);
  }
#endif
  return dest;
}


CImg<> apply_matirf_multi_index_operator(const CImgList<> & h, const CImg<int> index_map, const CImg<> & src) {
  CImg<> dest(src.width(), src.height(), h[0].height(), 1, 0);
#if 0
#pragma omp parallel for schedule(dynamic)
  cimg_forXY(src,x,y) {
    CImg<> f = h[index_map(x,y)] * src.get_crop(x,y,0,x,y,src.depth()-1).unroll('y');
    cimg_forZ(dest, z) dest(x,y,z) = f(z);
  }
#else
#pragma omp parallel for schedule(dynamic)
  cimg_forXY(src,x,y) {
    const int l = index_map(x,y);
    cimg_forZ(dest, theta) cimg_forZ(src, z)
      dest(x,y,theta) += h[l](z,theta) * src(x,y,z);
  }
#endif
  return dest;
}
