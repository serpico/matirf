// -*- mode: c++; -*-
/**
 *
 * \file operator.h
 *
 * \section licence_sec Licence
 *
 * Copyright Jérôme Boulanger (2013)
 *
 * jerome.boulanger@curie.fr
 *
 * This software is a computer program whose purpose is to reconstruct
 * images acquired in multi-angle total internal reflection microscopy.
 *
 * This software is governed by the CeCILLlicense under French law and
 * abiding by the rules of distribution of free software.  You can use,
 * modify and/ or redistribute the software under the terms of the
 * CeCILL license as circulated by CEA, CNRS and INRIA at the following
 * URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to
 * copy, modify and redistribute granted by the license, users are
 * provided only with a limited warranty and the software's author, the
 * holder of the economic rights, and the successive licensors have
 * only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks
 * associated with loading, using, modifying and/or developing or
 * reproducing the software by the user in light of its specific
 * status of free software, that may mean that it is complicated to
 * manipulate, and that also therefore means that it is reserved for
 * developers and experienced professionals having in-depth computer
 * knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions
 * enabling the security of their systems and/or data to be ensured
 * and, more generally, to use and operate it in the same conditions
 * as regards security.

 * The fact that you are presently reading this means that you have
 * had knowledge of the CeCILL license and that you accept its terms.
 *
 **/

#pragma once

#include "CImg.h"
#include "parameters.h"

using namespace cimg_library;

//! TIRF critical angle in degree
/**
   \param glass_index index of the glass
   \param medium_index index of the medium
   \return \f$ \mathrm{asin} (n_{medium} / n_{glass}) \f$ in degree
**/
float compute_tirf_critical_angle(const float glass_index, const float medium_index);

//! Compute the TIRF critical angle in degree
/**
   \param param tirf parameters
**/
float compute_tirf_critical_angle(const matirf_parameter &param);

//! Compute the TIRF maximum angle
/**
   \param oil_index index of the oil
   \param numerical_aperture numerical aperture of the objective
**/
float compute_tirf_max_angle(const float oil_index, const float numerical_aperture);

//! Compute the TIRF maximum angle
/**
   \param param tirf parameters
**/
float compute_tirf_max_angle(const matirf_parameter &param);

//! Compute angle corresponding to a given penetration depth
/**
   \param depth_nm penetration depth
   \param wavelength_nm wavelength
   \param glass_index index of the glass
   \param medium_index index of the medium
**/
float depth_to_angle(const float depth_nm, const float wavelength_nm,
		    const float glass_index, const float medium_index);

float depth_to_angle(const float depth_nm, const matirf_parameter & param);

//! Compute the penetration depth in nm for a given angle
/**
   \param angle_in_degree incidence angle in degree
   \param param matirf acquisition parameters
**/
float compute_penetration_depth_nm(const float angle_in_degree, const matirf_parameter & param);

//! return the TIRF operator matrix from physical parameters
/**
   \param depths values of depth as an 1D array containing beginning/end of pixel values
   \note the operator is a matrix of size angles.size() X depth.size() - 1
**/
CImg<> compute_matirf_operator(const CImg<> & depths, const CImg<> & angles,
			       const float wavelength_nm, const float glass_index,
			       const float medium_index, const float oil_index,
			       const float numerical_aperture, const float beam_divergence_in_degree,
			       const float precision=.01);

//! return the TIRF operator matrix from physical parameters
CImg<> compute_matirf_operator(const matirf_parameter & param,
			       const int nz, const float z0, const float z1,
			       const float precision=.01);

//! Apply the multi-angle TIRF operator
/**
   \param h the operator matrix
   \param src input data as a 3D image
   \return H * src taken along the 3D dimension
**/
CImg<> apply_matirf_operator(const CImg<> & h, const CImg<> & src);


//! Apply the multi-angle multi-index TIRF operator
/**
   \param h  list of operator matrix
   \param map 2D map of index of operator matrix
   \param src input data as a 3D image
   \return H * src taken along the 3D dimension
**/
CImg<> apply_matirf_multi_index_operator(const CImgList<> & h, const CImg<int> map, const CImg<> & src);
