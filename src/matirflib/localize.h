// -*- mode: c++; -*-
/**
 *
 * \file localize.h
 *
 * \section licence_sec Licence
 *
 * Copyright Jérôme Boulanger (2013)
 *
 * jerome.boulanger@curie.fr
 *
 * This software is a computer program whose purpose is to reconstruct
 * images acquired in multi-angle total internal reflection microscopy.
 *
 * This software is governed by the CeCILLlicense under French law and
 * abiding by the rules of distribution of free software.  You can use,
 * modify and/ or redistribute the software under the terms of the
 * CeCILL license as circulated by CEA, CNRS and INRIA at the following
 * URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to
 * copy, modify and redistribute granted by the license, users are
 * provided only with a limited warranty and the software's author, the
 * holder of the economic rights, and the successive licensors have
 * only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks
 * associated with loading, using, modifying and/or developing or
 * reproducing the software by the user in light of its specific
 * status of free software, that may mean that it is complicated to
 * manipulate, and that also therefore means that it is reserved for
 * developers and experienced professionals having in-depth computer
 * knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions
 * enabling the security of their systems and/or data to be ensured
 * and, more generally, to use and operate it in the same conditions
 * as regards security.

 * The fact that you are presently reading this means that you have
 * had knowledge of the CeCILL license and that you accept its terms.
 *
 **/

#include "parameters.h"
#include "CImg.h"
using namespace cimg_library;

CImg<> bead_model(const float z, const float fwhm, const CImg<> &depths, const CImg<> & H);

//! Estimate the depth of a bead like object from a multi-angle image stack
CImg<> estimate_depth_from_matirf(const CImg<> & coords,
				  const CImg<> & data,
				  const matirf_parameter & param,
				  const float size,
				  const float z0=0,
				  const float z1=400,
				  const float nz=400);

//! Estimate the depth of a bead like object from a multi-angle image stack
CImg<> estimate_depth_from_matirf(const CImg<> & coords,
				  const CImg<> & data,
				  const CImg<> & H,
				  const float fhwm,
				  const CImg<> & depths);

//! Print localization results
void print_localization_result(const CImg<> & ncoords);

//! Display localization results
void display_localization_result(const CImg<> & data, const CImg<> & coords);
