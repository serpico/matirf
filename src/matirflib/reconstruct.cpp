/**
 *
 * \file reconstruct.cpp
 *
 * \section licence_sec Licence
 *
 * Copyright Jérôme Boulanger (2013)
 *
 * jerome.boulanger@curie.fr
 *
 * This software is a computer program whose purpose is to reconstruct
 * images acquired in multi-angle total internal reflection microscopy.
 *
 * This software is governed by the CeCILLlicense under French law and
 * abiding by the rules of distribution of free software.  You can use,
 * modify and/ or redistribute the software under the terms of the
 * CeCILL license as circulated by CEA, CNRS and INRIA at the following
 * URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to
 * copy, modify and redistribute granted by the license, users are
 * provided only with a limited warranty and the software's author, the
 * holder of the economic rights, and the successive licensors have
 * only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks
 * associated with loading, using, modifying and/or developing or
 * reproducing the software by the user in light of its specific
 * status of free software, that may mean that it is complicated to
 * manipulate, and that also therefore means that it is reserved for
 * developers and experienced professionals having in-depth computer
 * knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions
 * enabling the security of their systems and/or data to be ensured
 * and, more generally, to use and operate it in the same conditions
 * as regards security.

 * The fact that you are presently reading this means that you have
 * had knowledge of the CeCILL license and that you accept its terms.
 *
 **/

#include "CImg.h"
using namespace cimg_library;
#include "lsqnonneg.h"
#include "cimgutils.h"
#include "reconstruct.h"
#include "operator.h"

// substract background and remove last image
void preprocess_matirf_images(CImg<> & data, matirf_parameter &param,
			      const bool flag) {
  if (param.angles.height() != data.depth()) {
    printf("\nWarning : while preprocessing number of angles (%d) "
	   "and number of planes (%d) do not match.\n",
	   param.angles.height(), data.depth());
    print_matirf_parameters(param);
  }
  CImg<> background(data.width(), data.height(), 1, 1, 0), ndata, nangles;
  const float theta1 = compute_tirf_max_angle(param);
  int k = 0, n = 0;
  cimg_forZ(data, z) {
    if (param.angles(z) > theta1) {
      background += data.get_shared_slice(z);
      n++;
    } else {
      ndata.append(data.get_slice(z), 'z');
      if (flag) {
	nangles.resize(1, nangles.size() + 1, 1, 1, 0);
	nangles(nangles.size() - 1) = param.angles(z);
      }
      k++;
    }
  }
  background /= (float) n;
  background.blur(5);
  cimg_forZ(ndata, z) {
    ndata.get_shared_slice(z) -= background;
  }
  if (flag) param.angles = nangles;
  data = ndata;
}

// select angles in the image
void select_matirf_angles(CImg<> & data, matirf_parameter &param,
			  const float theta0, const float theta1,
			  const bool flag) {
  if (param.angles.height() != data.depth()) {
    printf("\nWarning : while preprocessing number of angles (%d) "
	   "and number of planes (%d) do not match.\n",
	   param.angles.height(), data.depth());
    print_matirf_parameters(param);
  }
  CImg<> ndata;
  CImg<> nangles;
  cimg_forZ(data, z) {
    if (param.angles(z) >= theta0 && param.angles(z) <= theta1 ) {
      ndata.append(data.get_slice(z), 'z');
      if (flag) {
	nangles.resize(1, nangles.size() + 1, 1, 1, 0);
	nangles(nangles.size()-1) = param.angles(z);
      }
    }
  }
  if (flag) param.angles = nangles;
  data = ndata;
}

void select_matirf_angles(CImg<> & data, matirf_parameter &param,
			  const char * angles_str, const bool flag) {
  if (angles_str != NULL) {
    float theta0 = 0, theta1 = 90;
    sscanf(angles_str, "%f:%f", &theta0, &theta1);
    select_matirf_angles(data, param, theta0, theta1, flag);
  }
}

CImg<> spectral_cutoff(const CImg<> & M, const float gamma) {
  CImg<> Mreg, f, U, S, V;
  M.SVD(U,S,V,false);
  S.max(1.0 / gamma);
  S.diagonal();
  V.transpose();
  return U * S * V;
}

CImg<> compute_reconstruction_residuals(const CImg<> & h, const CImg<> & f, const CImg<>& g) {
  return g - apply_matirf_operator(h, f);
}

float compute_reconstruction_error_norm(const CImg<> & h, const CImg<> & f, const CImg<>& g) {
  return std::sqrt(apply_matirf_operator(h, f).MSE(g));
}

CImg<> compute_multi_index_reconstruction_residuals(const CImgList<> & h,
						    const CImg<int> & map,
						    const CImg<> & f,const CImg<>& g) {
  return g - apply_matirf_multi_index_operator(h, map, f);
}

float compute_multi_index_reconstruction_error_norm(const CImgList<> & h,
						    const CImg<int> & map,
						    const CImg<> & f,const CImg<>& g) {
  return std::sqrt(apply_matirf_multi_index_operator(h, map, f).MSE(g));
}

// \lambda_xy dx^2+dy^2 + lambda_z dz^2
float compute_norm_gradient(const CImg<> & f, const float lambda_xy, const float lambda_z){
  CImg_3x3x3(I,float);
  float s = 0;
  cimg_for3x3x3(f,x,y,z,0,I,float) {
    float dx = Incc - Iccc, dy = Icnc - Iccc, dz = Iccn - Iccc;
    s += lambda_xy * (dx * dx + dy * dy) + lambda_z * dz * dz;
  }
  return s / (float) f.size();
}

// Distance to positivity
float compute_distance_positivity(const CImg<> & f) {
  float d = 0;
  cimg_for(f,ptr,float) {
    const float val = *ptr - cimg::max(*ptr, 0.f);
    d += val * val;
  }
  return 0.5 * d /  (float) f.size();
}

// Prox squared distance 1/2 d^2_C(x) -> 1/2 (x + P_c(x))
CImg<> proxd2(const CImg<> & src) {
  CImg<> dest(src);
  cimg_for(dest,ptr,float) {
    *ptr = 0.5f * (*ptr + cimg::max(*ptr, 0.f));
  }
  return dest;
}

// criterion L2-L2 + positivity
//  ||Hf-g||^2 + \lambda ||\nabla f||^2 + 1/2 d(f>0)
float compute_criterion_l2l2p(const CImg<> & h, const CImg<> & f, const CImg<>& g,
			      const float lambda_xy, const float lambda_z) {
  const float
    a = compute_reconstruction_error_norm(h, f, g),
    b = compute_norm_gradient(f, lambda_xy, lambda_z),
    c = compute_distance_positivity(f);
  return a * a + b + c;
}

//! Anisotropic Tikhonov denoising using PDE approach
/**
   Minimize \f$ \|u - u_0\|^2 + \lambda_xy \| \nabla_xy u \|^2 + \lambda_z \| \nabla_z u \|^2 \f$
   using the Euler-Lagrange equation
   \f$ u_{n+1} = u_n + dt/\lambda ( u_0-u_n+\lambda \Delta u_n )\f$
**/
template<typename T>
CImg<T> denoise_tikhonov_anisotropic(const CImg<T> & img,
				     const float lambda_xy = 1,
				     const float lambda_z = 1,
				     const float dt = 10.0,
				     const int max_iter = 100){
  if (img.is_empty()) throw CImgArgumentException("denoise_tikonov: empty image.");
  CImg<T> dest(img), veloc(img.width(), img.height(), img.depth(), img.spectrum(), 0);
  CImg_3x3x3(I,float);
  for (int iter = 0; iter < max_iter; ++iter) {
    cimg_forC(dest, c) cimg_for3x3x3(dest,x,y,z,c,I,float) {
      veloc(x,y,z,c) += img(x,y,z,c) - dest(x,y,z,c)
	+ lambda_xy*(Incc + Ipcc + Icnc + Icpc) + lambda_z * (Iccn + Iccp)
	- (4 * lambda_xy + 2 * lambda_z) * Iccc;
    }
    float m, M = veloc.max_min(m);
    const double xdt = dt/(M - m);
    dest += xdt * veloc;
  }
  return dest;
}

// Apply the Mean Curvature Motion (Inspired from D. Tschumperle)
template<typename T>
CImg<T> denoise_mean_curvature(const CImg<T>& img, const float lambda=1,
			       const float dt=1.0f, const int max_iter=10) {
  CImg<> u(img);
  CImg<float> velocity(img.width(),img.height(),img.depth(),img.spectrum());
  CImg_3x3x3(I,float);
  for (int iter = 0; iter < max_iter; ++iter) {
    float *ptrd = velocity.data();
    cimg_for3x3x3(img,x,y,z,0,I,float) {
      const float
        ix = (Incc - Ipcc)/2,
        iy = (Icnc - Icpc)/2,
        iz = (Iccn - Iccp)/2,
        norm = (float)std::sqrt(1e-5f + ix*ix + iy*iy + iz*iz),
        ixx = Incc + Ipcc - 2*Iccc,
        ixy = (Ippc + Innc - Inpc - Ipnc)/4,
        ixz = (Ipcp + Incn - Incp - Ipcn)/4,
        iyy = Icnc + Icpc - 2*Iccc,
        iyz = (Icpp + Icnn - Icnp - Icpn)/4,
        izz = Iccn + Iccp - 2*Iccc,
        a = ix/norm,
        b = iy/norm,
        c = iz/norm,
        inn = a*a*ixx + b*b*iyy + c*c*izz + 2*a*b*ixy + 2*a*c*ixz + 2*b*c*iyz,
        veloc = ixx + iyy + izz - inn;
      *(ptrd++) = img(x,y,z) - u(x,y,z) + lambda * veloc;
    }
    float m, M = velocity.max_min(m);
    const double xdt = dt/(M - m);
    u += xdt * velocity;
  }
  return u;
}

const char * get_smoother_str(const int id) {
  switch(id) {
  case 1: return "Gaussian";
  case 2: return "Tikhonov";
  case 3: return "Total-Variation";
  case 4: return "Mean Curvature Flow";
  case 5: return "Translation invariant wavelet thresholding";
  case 6: return "CHLPCA";
  }
  return "unknown";
}

const char * get_algorithm_str(const int id) {
  switch(id) {
  case -1: return "landweber";
  case 0: return "nnls";
  case 1: return "ppxa";
  case 2: return "ppxa blind";
  case 3: return "ppxa blind adaptive";
  }
  return "unknown";
}

CImg<> prox_smooth(const CImg<> & img, const int id=1,
		   const float lambda_xy=1, const float lambda_z=1) {
  switch(id) {
  case 1: return img.get_blur(lambda_xy, lambda_xy, lambda_z, true, true);
  case 2: return denoise_tikhonov_anisotropic(img, lambda_xy, lambda_z, 5, 5);
  case 3: return denoise_chambolle_total_variation(img, lambda_xy);
  case 4: return denoise_mean_curvature(img);
  case 5: return denoise_tiwt(img,2,3);
  case 6: return denoise_chlpca(img,2,2,1,3,3,1,5,6,1,1,1);
  }
  return img;
}

// Inner step of the ppxa algorithm using different weights
// p_{k} = \sum w_i p_{i,k}
// u_{i,k+1} = u_{i,k} + \lambda ( 2 p_{k} - x_k - p_{i,k} )
// x_{k+1} = x_k + \lambda (p_k - x_k)
void ppxa_inner_step(CImgList<> & p, CImgList<> & u, CImg<> & dest,
		     const CImg<> weights, const float lambda = 1.9f) {
  const size_t sz = dest.size();
  CImg<> nweights(weights / weights.sum());
#pragma omp parallel for schedule(dynamic)
  for (size_t off = 0; off < sz; ++off) {
    float pl = 0;
    const float desti = dest(off);
    cimglist_for(p, j) pl += nweights(j) * p[j](off);
    cimglist_for(u, j) u[j](off) +=  lambda * (2.0f * pl - desti - p[j](off));
    dest(off) += lambda * (pl - desti);
  }
}

// Inner step of the ppxa algorithm with weights equal to 1/l
void ppxa_inner_step(CImgList<> & p, CImgList<> & u, CImg<> & dest,
		     const float lambda = 1.9f) {
  CImg<> weights(p.width(),1,1,1,1);
  ppxa_inner_step(p, u, dest, weights, lambda);
}

// Perform a space time smoothing as a approximation of prox for
// space-time regularization
void inplace_space_time_smoothing(CImgList<> &img, const int start, const int end,
				  const float lambda_xy, const float lambda_z, const int proxid = 1) {
  for (int i = start; i <= end; i++)  img[i] = prox_smooth(img[i], proxid, lambda_xy, lambda_z);
  CImgList<> tmp(end - start + 1, img[0].width(), img[0].height(), img[0].depth(), 1, 0);
  for (int i = start; i <= end; i++) {
    const int ip = cimg::max(0, i - 1), in = cimg::min(end - 1, i + 1);
    const size_t sz = img[i].size();
#pragma omp parallel for schedule(dynamic)
    for (size_t off = 0; off < sz; ++off) {
      tmp[i - start](off) = 0.25 * img[ip](off) + 0.5 * img[i](off) + 0.25 * img[in](off);
    }
  }
#pragma omp parallel for schedule(dynamic)
  for (int i = start; i <= end; i++) img[i] = tmp[i - start];
}

CImg<> matirf_estimate_psf(const CImg<> &h0, const CImg<> &g, const CImg<> & f,
			   const float scale=.25, const float gamma=1e3){
  CImg<>
    gr = g.get_resize(-100*scale,-100*scale,-100,1,5),
    fr = f.get_resize(-100*scale,-100*scale,-100,1,5),
    X(fr.depth(),fr.width() * fr.height()),
    Y(gr.depth(),gr.width() * gr.height());
  int i = 0;
  cimg_forXY(fr,x,y){
    cimg_forZ(fr, z) X(z, i) = fr(x, y, z);
    cimg_forZ(gr, z) Y(z, i) = gr(x, y, z);
    i++;
  }
  // Y = X h
  CImg<> h;
  // (XtT+aI)^(-1)*XtY
  CImg<> XtX = X.get_transpose()*X, I = XtX.get_identity_matrix();
  h = (I + gamma* XtX).pseudoinvert() * (h0.get_transpose() + X.get_transpose() * Y);
  h.transpose();
  h.max(0.f);
  CImg<double> U,S,V;
  h.SVD(U,S,V);
  h /=  S.max();
  return h;
}

//! reshape the 3d estimate (nx,ny,nz) as a 2d image of dimension (z,nx*ny)
CImg<> reshape_matirf(const CImg<> &src) {
  CImg<> dest(src.depth(), src.width() * src.height());
  int i = 0;
  cimg_forXY(src, x, y) {
    cimg_forZ (src,z) {
      dest(z, i) = src(x, y, z);
    }
    i++;
  }
  return dest;
}

//! compute the resolvant \f$ (I+A'A)^{-1} \f$ of the operator
CImg<> get_resolvant(const CImg<> & A, const float gamma) {
  return (CImg<>::identity_matrix(A.width()) + gamma * A.get_transpose() * A).invert();
}

//! Project the operator on the space of operator of unit norm
CImg<> prox_L(CImg<> &A) {
  CImg<double> U,S,V;
  A.SVD(U,S,V);
  return A / S.max();
}

void display_iteration(CImgDisplay &disp, const CImg<> h, const CImg<> f, const CImg<> g,
		       const float lambda_xy, const float lambda_z, const int iter, const int mode) {
  if (mode > 1) disp.display(f).set_title("n:%d", iter);
  if (mode > 2)
    printf("%d\t%f\n", iter, compute_criterion_l2l2p(h, f, g, lambda_xy, lambda_z));
}

CImg<> reconstruct_matirf_nnls(const CImg<> & h, const CImg<> &data, const float gamma) {
  CImg<> hreg = spectral_cutoff(h, gamma), dest(data.width(), data.height(), h.width(), 1, 0);
#pragma omp parallel for collapse(2)
  cimg_forXY(data,x,y) {
    CImg<> f = lsqnonneg(hreg, data.get_crop(x,y,0,x,y,data.depth()-1).unroll('y'), 2);
    cimg_forZ(dest, z) dest(x,y,z) = f(z);
  }
  return dest;
}

// landweber iterations to minimize ||Hf-g||^2 + \lambda ||\nabla||^2 + .5 d^2(f>0)
// Forward-Backward type of algorithm
// The gradient in z direction can be less penalized using lambda_z < lambda_xy
CImg<> reconstruct_matirf_landweber(const CImg<> & h, const CImg<> &data,
				    const float lambda_xy, const float lambda_z,
				    const float dt, const int max_iter, const int mode) {
  CImg<> dest(data.width(), data.height(), h.width(), 1, 0);
  CImg<> HtG = apply_matirf_operator(h.get_transpose(), data), HtH = h.get_transpose() * h;
  CImgDisplay disp;
  CImg_3x3x3(I,float);
  for (int iter = 0; iter < max_iter; ++iter) {
    // veloc = -gradient(||Hf-g||^2 + \lambda ||\nabla||^2)
    CImg<> HtHf = apply_matirf_operator(HtH, dest);
    CImg<> veloc(data.width(), data.height(), h.width(), 1, 0);
    cimg_for3x3x3(dest,x,y,z,0,I,float)
      veloc(x,y,z) += HtG(x,y,z) - HtHf(x,y,z)
      + lambda_xy * (Incc + Ipcc + Icnc + Icpc)
      + lambda_z * (Iccn + Iccp) - (4 * lambda_xy + 2 * lambda_z) * Iccc;
    float m, M = veloc.max_min(m);
    const double xdt = dt/(M - m);
    dest += xdt * veloc;
    dest = proxd2(dest);
    display_iteration(disp, h, dest, data, lambda_xy, lambda_z, iter, mode);
  }
  return dest;
}

CImg<> reconstruct_matirf_ppxa(const CImg<> & h, const CImg<> &data,
			       const float lambda_xy, const float lambda_z,
			       const float gamma, const int max_iter,
			       const int regularization, const int mode) {
  CImgList<> p(3), u(3);
  CImg<>
    P = get_resolvant(h, gamma),
    HtG = gamma * apply_matirf_operator(h.get_transpose(), data),
    dest = apply_matirf_operator(P, HtG);
  cimglist_for(u, j) { u[j] = dest; }
  CImgDisplay disp;
  for (int iter = 0; iter < max_iter; iter++) {
    p[0] = apply_matirf_operator(P, u[0] + HtG);
    p[1] = proxd2(u[1]);
    p[2] = prox_smooth(u[2], regularization, lambda_xy, lambda_z);
    ppxa_inner_step(p, u, dest);
    display_iteration(disp, h, dest, data, lambda_xy, lambda_z, iter, mode);
  }
  return dest;
}

CImg<> reconstruct_matirf_multi_index_ppxa(const CImgList<> & h, const CImg<int> map,
					   const CImg<> &data,
					   const float lambda_xy, const float lambda_z,
					   const float gamma, const int max_iter,
					   const int proxid, const int mode) {
  CImgList<> P(h.width()), Ht(h.width()), p(3), u(3);
  cimglist_for(h, l) {
    Ht[l] = h[l].get_transpose();
    P[l] = get_resolvant(h[l], gamma);
  }
  CImg<>
    HtG = gamma * apply_matirf_multi_index_operator(Ht, map, data),
    dest = apply_matirf_multi_index_operator(P, map, HtG);
  cimglist_for(u, j) { u[j] = dest; }
  CImgDisplay disp;
  for (int iter = 0; iter < max_iter; iter++) {
    p[0] = apply_matirf_multi_index_operator(P, map, u[0] + HtG);
    p[1] = proxd2(u[1]);
    p[2] = prox_smooth(u[2], proxid, lambda_xy, lambda_z);
    ppxa_inner_step(p, u, dest);
    display_iteration(disp, h[0], dest, data, lambda_xy, lambda_z, iter, mode);
  }
  return dest;
}

CImgList<> reconstruct_matirf_multi_frame_ppxa(const CImg<> & h, const CImgList<> & data,
					       const float lambda_xy, const float lambda_z,
					       const float gamma, const int max_iter,
					       const int regularization) {

  CImgList<> hl(h);
  CImgList<int> map(data.width(), data[0].width(), data[0].height(), 1, 1, 0);
  return reconstruct_matirf_multi_index_multi_frame_ppxa(hl, map, data,
							 lambda_xy, lambda_z,
							 gamma, max_iter,
							 regularization);
}

CImgList<> reconstruct_matirf_multi_index_multi_frame_ppxa(const CImgList<> & h, const CImgList<int> map,
							   const CImgList<> &data,
							   const float lambda_xy, const float lambda_z,
							   const float gamma, const int max_iter,
							   const int regularization) {
  CImgList<> P(h.width()), Ht(h.width());
  cimglist_for(h, l) {
    Ht[l] = h[l].get_transpose();
    P[l] = get_resolvant(h[l], gamma);
  }

  CImgList<> dest(data.width()), HtG(data.width());
  cimglist_for(dest, j) {
    HtG[j] = gamma * apply_matirf_multi_index_operator(Ht, map[j], data[j]);
    dest[j]= apply_matirf_multi_index_operator(P, map[j], HtG[j]);
  }

  CImgList<> p(3 * data.width()), u(3 * data.width());
  for (int i = 0; i < 3; ++i) cimglist_for(dest, j) { u[j + i * dest.width()] = dest[j]; }

  for (int iter = 0; iter < max_iter; iter++) {

    cimglist_for(dest, j) {
      const int idx0 = j, idx1 = j + data.width(), idx2 = j + 2 * data.width();
      p[idx0] = apply_matirf_multi_index_operator(P, map[j], u[j] + HtG[j]);
      p[idx1] = proxd2(u[idx1]);
      p[idx2] = u[idx2];
    }

    inplace_space_time_smoothing(p, 2 * data.width(), 3 * data.width() - 1,
				 lambda_xy, lambda_z, regularization);

    cimglist_for(dest, j) {
      const int idx0 = j, idx1 = j + data.width(), idx2 = j + 2 * data.width();
      CImgList<> Pj(p[idx0], p[idx1], p[idx2], true), Uj(u[idx0], u[idx1], u[idx2], true);
      ppxa_inner_step(Pj, Uj, dest[j]);
    }
  }

  return dest;
}

CImg<> reconstruct_matirf_blind_ppxa(CImg<> &h, const CImg<> & data,
				     const float lambda_xy, const float lambda_z,
				     const float gamma, const int max_iter,
				     const int regularization, const int mode) {
  if (data.width() * data.height() < h.width())
    printf("reconstruct_matirf_blind_ppxa: not enought samples\n");
  const float gamma2 = 1;
  CImgList<> p1(3), u1(3), p2(3), u2(3);
  CImg<> f(data.width(), data.height(), h.width(), 1, 0),
    vecg = reshape_matirf(data), weights_h = CImg<>::vector(1,2,2);
  cimglist_for(u1, j) { u1[j] = f; }
  cimglist_for(u2, j) { u2[j] = h; }
  CImgDisplay disp1, disp2;
  for (int iter = 0; iter < max_iter; iter++) {
    CImg<>
      P1 = get_resolvant(h, gamma),
      HtG = gamma * apply_matirf_operator(h.get_transpose(), data);
    p1[0] = apply_matirf_operator(P1, u1[0] + HtG);
    p1[1] = u1[1].max(0.f);
    p1[2] = prox_smooth(u1[2], regularization, lambda_xy, lambda_z);
    ppxa_inner_step(p1, u1, f);
    CImg<>
      vecf = reshape_matirf(f),
      P2 = get_resolvant(vecf, weights_h(0) * gamma2),
      FtG = weights_h(0) * gamma2 * apply_matirf_operator(vecf.get_transpose(), vecg);
    p2[0] = apply_matirf_operator(P2, u2[0] + FtG);
    p2[1] = weights_h(1) * u2[1].max(0.f);
    p2[2] = weights_h(2) * prox_L(u2[2]);
    ppxa_inner_step(p2, u2, h, weights_h, 0.01f);
    if (mode > 1) {
      h.get_resize(-400,-400).display(disp1);
      f.display(disp2);
      if (disp1.is_resized()) disp1.resize();
      if (disp2.is_resized()) disp2.resize();
    }
  }
  return f;
}

CImg<> reconstruct_matirf_local_blind_ppxa(const CImg<> &h, const CImg<> & data,
					   const float lambda_xy, const float lambda_z,
					   const float gamma, const int max_iter,
					   const int regularization,
					   const int p, const int mode) {
  CImg<> map(data.width(), data.height(),1,1,0);
  CImgList<> hs;
  hs.push_back(h);
  return reconstruct_matirf_multi_index_local_blind_ppxa(hs, map, data,
							 lambda_xy, lambda_z,
							 gamma, max_iter,
							 regularization, p, mode);
}

CImg<> reconstruct_matirf_multi_index_local_blind_ppxa(const CImgList<> &h, const CImg<int> & map,
						       const CImg<> & data,
						       const float lambda_xy, const float lambda_z,
						       const float gamma, const int max_iter,
						       const int regularization,
						       const int p, const int mode) {
  if (map.max() >= h.width())
    throw CImgException("reconstruct_matirf_multi_index_local_blind_ppxa(..)\n"
			" index map does not match the list of operators\n"
			" map.max()=%d and h.width()=%d", map.max(), h.width());

  CImg<> f(data.width(), data.height(), h[0].width(), 1, 0),
    sf(data.width(), data.height(), 1, 1, 0);

  CImgDisplay disp;
#pragma omp parallel for collapse(2)
  cimg_for_stepXY(data, x, y, p) {
    const int x0 = cimg::max(0, x - p), y0 = cimg::max(0, y - p),
      x1 = cimg::min(data.width()-1, x + p), y1 = cimg::min(data.height()-1, y + p);
    if (x1 > x0 && y1 > y0) {
      CImg<> lg = data.get_crop(x0,y0,x1,y1), lh = h[map(x,y)],
	lf = reconstruct_matirf_blind_ppxa(lh, lg, lambda_xy, lambda_z, gamma, max_iter, regularization),
	lr = mean(compute_reconstruction_residuals(lh, lf, lg).sqr(), 'z');
#pragma omp critical (paste)
      {
	for (int yi = y0; yi <= y1; ++yi)
	  for (int xi = x0; xi <= x1; ++xi) {
	    const float w = 1.0f / cimg::max(1e-6, lr(xi - x0, yi - y0));
	    cimg_forZ(f, z) f(xi, yi, z) += w * lf(xi - x0, yi - y0, z);
	    sf(xi, yi) += w;
	  }
	if (mode > 1) sf.display(disp);
      }
    }
  }

  cimg_forXY(f,x,y) {
    const float val = sf(x,y);
    if (val > 1e-12) cimg_forZ(f, z) f(x,y,z) /= val;
  }

  return f;
}


CImg<> reconstruct_matirf(CImg<> &h, const CImg<> & data,
			  const float lambda_xy, const float lambda_z,
			  const float gamma, const int max_iter,
			  const int algo, const int regularization,
			  const int block_size, const int mode) {
  CImg<> f;
  switch (algo) {
  case -1:
    if (regularization != 1 && regularization != 2)
      throw CImgException("reconstruct_matirf() invalide regularization "
			  "(%d) for landweber reconstruction. Only regu"
			  "larization 1 and 2 are available.",
			  regularization);
    f = reconstruct_matirf_landweber(h, data, lambda_xy, lambda_z,
				     gamma, max_iter, mode);
    break;
  case 0:
    f = reconstruct_matirf_nnls(h, data, gamma);
    break;
  case 1:
    f = reconstruct_matirf_ppxa(h, data, lambda_xy, lambda_z,
				gamma, max_iter, regularization, mode);
    break;
  case 2:
    f = reconstruct_matirf_blind_ppxa(h, data, lambda_xy, lambda_z,
				      gamma, max_iter, regularization, mode);
    h.max(0.f);
    break;
  case 3:
    f = reconstruct_matirf_local_blind_ppxa(h, data, lambda_xy, lambda_z,
					    gamma, max_iter, block_size,
					    regularization, mode);
    break;
  default:
    throw CImgException("matirf reconstruct : invalid option -algo %d\n", algo);
  }
  return f.max(0.f);
}

CImg<> reconstruct_matirf(const CImg<> &h, const CImg<> & data,
			  const float lambda_xy, const float lambda_z,
			  const float gamma, const int max_iter,
			  const int algo, const int regularization,
			  const int block_size, const int mode) {
  CImg<> tmp(h, false);
  return reconstruct_matirf(tmp, data, lambda_xy, lambda_z, gamma, max_iter,
			    algo, regularization, block_size, mode);
}


CImg<> reconstruct_matirf(CImgList<> &h, const CImg<int> &map, const CImg<> & data,
			  const float lambda_xy, const float lambda_z,
			  const float gamma, const int max_iter,
			  const int algo, const int regularization,
			  const int block_size, const int mode) {
  CImg<> f;
  if (h.width() == 1) {
    f = reconstruct_matirf(h[0], data, lambda_xy, lambda_z, gamma, max_iter,
			      algo, regularization, block_size, mode);
  } else {
    switch (algo) {
    case 1:
      f = reconstruct_matirf_multi_index_ppxa(h, map, data,
					      lambda_xy, lambda_z,
					      gamma, max_iter,
					      regularization, mode);
      break;
    case 3:
      f = reconstruct_matirf_multi_index_local_blind_ppxa(h, map, data,
							  lambda_xy, lambda_z,
							  gamma, max_iter, regularization,
							  block_size, mode);
      break;
    default:
      throw CImgException("matirf reconstruct : invalid option -algo %d\n", algo);
    }
  }
  return f.max(0.f);
}

CImg<> reconstruct_matirf(const CImgList<> &h, const CImg<int> &map, const CImg<> & data,
			  const float lambda_xy, const float lambda_z,
			  const float gamma, const int max_iter,
			  const int algo, const int regularization,
			  const int block_size, const int mode) {
  CImgList<> tmp(h, false);
  return reconstruct_matirf(tmp, map, data, lambda_xy, lambda_z, gamma, max_iter,
			    algo, regularization, block_size, mode);
}
