// -*- mode: c++; -*-
/**
 *
 * \file reconstruct.h
 *
 * \section licence_sec Licence
 *
 * Copyright Jérôme Boulanger (2013)
 *
 * jerome.boulanger@curie.fr
 *
 * This software is a computer program whose purpose is to reconstruct
 * images acquired in multi-angle total internal reflection microscopy.
 *
 * This software is governed by the CeCILLlicense under French law and
 * abiding by the rules of distribution of free software.  You can use,
 * modify and/ or redistribute the software under the terms of the
 * CeCILL license as circulated by CEA, CNRS and INRIA at the following
 * URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to
 * copy, modify and redistribute granted by the license, users are
 * provided only with a limited warranty and the software's author, the
 * holder of the economic rights, and the successive licensors have
 * only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks
 * associated with loading, using, modifying and/or developing or
 * reproducing the software by the user in light of its specific
 * status of free software, that may mean that it is complicated to
 * manipulate, and that also therefore means that it is reserved for
 * developers and experienced professionals having in-depth computer
 * knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions
 * enabling the security of their systems and/or data to be ensured
 * and, more generally, to use and operate it in the same conditions
 * as regards security.

 * The fact that you are presently reading this means that you have
 * had knowledge of the CeCILL license and that you accept its terms.
 *
 **/
#pragma once

#include "CImg.h"
#include "parameters.h"

using namespace cimg_library;

//! Preprocess multi-angle TIRF image stack
/**
   \param flag indicate if the angle list should also be modified
**/
void preprocess_matirf_images(CImg<> & data, matirf_parameter &param, const bool flag=true);

//! Select angles in a range
/**
   \param flag indicate if the angle list should also be modified
**/
void select_matirf_angles(CImg<> & data, matirf_parameter &param,
			  const float theta0, const float theta1,
			  const bool flag=true);

//! Select angles in a range
/**
   \param data matirf data as a 3D image
   \param param multi angle acquisition parameters
   \param angles_str a string "beg:end" indicating the angle range
   \param flag indicate if the angle list should also be modified
**/
void select_matirf_angles(CImg<> & data, matirf_parameter &param,
			  const char * angles_str, const bool flag=true);

//! Compute the reconstruction residuals
/**
   \param h opertor as a matrix
   \param f estimate
   \param g data
**/
CImg<> compute_reconstruction_residuals(const CImg<> & h, const CImg<> & f, const CImg<>& g);

//! Compute the reconstruction error norm
/**
   \param h opertor as a (\f$ n_{planes} \times n_{angles} \f$ matrix)
   \param f estimate with depth = \f$n_{planes}\f$
   \param g data with depth = \f$n_{angles}\f$
**/
float compute_reconstruction_error_norm(const CImg<> & h, const CImg<> & f, const CImg<>& g);

//! Compute the reconstruction residuals for a multi-index model
/**
   \param h opertor as a list of matrix
   \param map CImg containing the index of the operator for each x,y location
   \param f estimate with depth = \f$n_{planes}\f$
   \param g data with depth = \f$n_{angles}\f$
**/
CImg<> compute_multi_index_reconstruction_residuals(const CImgList<> & h,
						    const CImg<int> & map,
						    const CImg<> & f,const CImg<>& g);

//!  Compute the multi-index TIRF reconstruction error norm
/**
   \param h opertor as a list of matrix
   \param map CImg containing the index of the operator for each x,y location
   \param f estimation with depth = \f$n_{planes}\f$
   \param g data with depth = \f$n_{angles}\f$
**/
float compute_multi_index_reconstruction_error_norm(const CImgList<> & h, const CImg<int> & map, const CImg<> & f, const CImg<>& g);

//! Reconstruct multi-angle TIRF image stack using NNLS algorithm
/**
   \param h  operator (\f$ n_{planes} \times n_{angles} \f$ matrix)
   \param data matirf data with depth = \f$n_{planes}\f$
   \param lambda cut-off regularization parameter
**/
CImg<> reconstruct_matirf_nnls(const CImg<> & h, const CImg<> &data, const float lambda);

//! Reconstruct multi-angle TIRF image stack using Lanweber algorithm
/**
   \param h operator (\f$ n_{planes} \times n_{angles} \f$ matrix)
   \param data matirf data with depth = \f$n_{planes}\f$
   \param lambda_xy regularization in xy
   \param lambda_xy regularization in z
   \param dt time step
   \param max_iter maximum number of iterations
   \param mode display informations or not

   Landweber iterations to minimize \f$ ||Hf-g||^2 + \lambda_xy ||\nabla_x f||^2 + \lambda_z ||\nabla_z f||^2 + .5 d^2(f>0) \f$
**/
CImg<> reconstruct_matirf_landweber(const CImg<> & h, const CImg<> &data,
				    const float lambda_xy, const float lambda_z,
				    const float dt = 1.0, const int max_iter = 1000,
				    const int mode = 0);

//! Reconstruct multi-angle TIRF image stack using PPXA algorithm
/**
   \param h operator (\f$ n_{planes} \times n_{angles} \f$ matrix)
   \param data matirf data with depth = \f$n_{planes}\f$
   \param lambda_xy regularization in xy
   \param lambda_xy regularization in z
   \param dt time step
   \param max_iter maximum number of iterations
   \param  regularization regularization id
   \param mode display informations or not

   PPXA to minimize \f$ ||Hf-g||^2 + \lambda_xy ||\nabla_x f||^2 + \lambda_z ||\nabla_z f||^2 + .5 d^2(f>0) \f$
**/
CImg<> reconstruct_matirf_ppxa(const CImg<> & h, const CImg<> &data,
			       const float lambda_xy, const float lambda_z,
			       const float gamma = 10, const int max_iter = 100,
			       const int regularization = 1, const int mode = 0);

//! Reconstruct multi-angle TIRF image stack using PPXA algorithm with a multi-index model
/**
   \param h list of operators  (\f$ n_{planes} \times n_{angles} \f$ matrix)
   \param map index of the operators
   \param data matirf data with depth = \f$n_{planes}\f$
   \param lambda_xy regularization in xy
   \param lambda_xy regularization in z
   \param dt time step
   \param max_iter maximum number of iterations
   \param regularization regularization type
   \param mode display informations or not

   PPXA to minimize \f$ ||Hf-g||^2 + \lambda_xy ||\nabla_x f||^2 + \lambda_z ||\nabla_z f||^2 + .5 d^2(f>0) \f$
   with H an indexed operator by the map 'map'.
**/
CImg<> reconstruct_matirf_multi_index_ppxa(const CImgList<> & h, const CImg<int> map,
					   const CImg<> &data,
					   const float lambda_xy, const float lambda_z,
					   const float gamma = 10, const int max_iter = 100,
					   const int regularization = 1, const int mode = 0);

//! Reconstruct multi-angle multi-frame TIRF image stack using PPXA algorithm with a multi-index model
/**
   \param h operators
   \param data data
   \param lambda_xy regularization in xy
   \param lambda_xy regularization in z
   \param gamma time step
   \param max_iter maximum number of iterations
   \param regularization regularization type

   PPXA to minimize \f$ ||Hf-g||^2 + \lambda_xy ||\nabla_x f||^2 + \lambda_z ||\nabla_z f||^2 + .5 d^2(f>0) \f$
   with H an indexed operator by the map 'map'.

**/
CImgList<> reconstruct_matirf_multi_frame_ppxa(const CImg<> & h, const CImgList<> & data,
					       const float lambda_xy, const float lambda_z,
					       const float gamma = 10, const int max_iter= 100,
					       const int regularization = 1);

//! Reconstruct multi-angle multi-frame TIRF image stack using PPXA algorithm with a multi-index model
/**
   \param h list of operators
   \param map index of the operators
   \param data data
   \param lambda_xy regularization in xy
   \param lambda_xy regularization in z
   \param gamma time step
   \param max_iter maximum number of iterations
   \param regularization regularization type

   PPXA to minimize \f$ ||Hf-g||^2 + \lambda_xy ||\nabla_x f||^2 + \lambda_z ||\nabla_z f||^2 + .5 d^2(f>0) \f$
   with H an indexed operator by the map 'map'.

**/
CImgList<> reconstruct_matirf_multi_index_multi_frame_ppxa(const CImgList<> & h, const CImgList<int> map,
							   const CImgList<> & data,
							   const float lambda_xy, const float lambda_z,
							   const float gamma = 10, const int max_iter= 100,
							   const int regularization = 1);

//! Reconstruct multi-angle TIRF image stack using PPXA algorithm and estimate the operator in parallel
/**
   \param h  operators (\f$ n_{planes} \times n_{angles} \f$ matrix)
   \param map index of the operators
   \param data matirf data with depth = \f$n_{planes}\f$
   \param lambda_xy regularization in xy
   \param lambda_xy regularization in z
   \param gamma time step
   \param max_iter maximum number of iterations
   \param mode display informations or not

   \note the operator is estimated as well as the intensity.
**/
CImg<> reconstruct_matirf_blind_ppxa(CImg<> &h, const CImg<> & data,
				     const float lambda_xy, const float lambda_z,
				     const float gamma, const int max_iter,
				     const int regularization = 1, const int mode = 0);

//! Reconstruct multi-angle TIRF image stack using PPXA algorithm and estimate the operator in parallel
/**
   \param h operator (\f$ n_{planes} \times n_{angles} \f$ matrix)
   \param data matirf data with depth = \f$n_{planes}\f$
   \param lambda_xy regularization in xy
   \param lambda_xy regularization in z
   \param gamma time step
   \param max_iter number of iterations
   \param regularization regularization id
   \param p size of the bloc (21x21 by default)
   \param mode  if mode>1 display iterations
   \note the operator is estimated on blocks of size 2p+1 x 2p+1
   partially overlapping. The estimations on the blocs are
   independants and the operators are not stored (would be a field of
   matrix). The overlap of blocs provides multiple estimates at a
   given point which are aggregated.
**/
CImg<> reconstruct_matirf_local_blind_ppxa(const CImg<> &h, const CImg<> & data,
					   const float lambda_xy, const float lambda_z,
					   const float gamma, const int max_iter,
					   const int regularization = 1, const int p = 15, const int mode = 0);


//! Reconstruct multi-angle TIRF image stack using PPXA algorithm and estimate the operator in parallel
/**
   \param h operator (\f$ n_{planes} \times n_{angles} \f$ matrix)
   \param data matirf data with depth = \f$n_{planes}\f$
   \param lambda_xy regularization in xy
   \param lambda_xy regularization in z
   \param gamma time step
   \param max_iter number of iterations
   \param regularization regularization id
   \param p size of the bloc (21x21 by default)
   \param mode  if mode>1 display iterations
   \note the operator is estimated on blocks of size 2p+1 x 2p+1
   partially overlapping. The estimations on the blocs are
   independants and the operators are not stored (would be a field of
   matrix). The overlap of blocs provides multiple estimates at a
   given point which are aggregated.

   The local operator is initialized using a map of operators
**/
CImg<> reconstruct_matirf_multi_index_local_blind_ppxa(const CImgList<> &h, const CImg<int> & map,
						       const CImg<> & data,
						       const float lambda_xy, const float lambda_z,
						       const float gamma, const int max_iter,
						       const int regularization = 1,
						       const int p = 15,const int mode = 0);

//! Reconstruct multi-angle TIRF image
/**

   \param h operator (\f$ n_{planes} \times n_{angles} \f$ matrix)
   \param data matirf data with depth = \f$n_{planes}\f$
   \param lambda_xy regularization in xy
   \param lambda_xy regularization in z
   \param gamma time step
   \param max_iter number of iterations
   \param algo algorithm (-1:landweber,0:nnls,1:ppxa)
   \param regularization regularization id
   \param p size of the bloc (21x21 by default)
   \param mode  if mode>1 display iterations

   Reconstruction Factory

   const version for the operator
**/
CImg<> reconstruct_matirf(const CImg<> &h, const CImg<> & data,
			  const float lambda_xy, const float lambda_z,
			  const float gamma, const int max_iter,
			  const int algo = 1, const int regularization = 1,
			  const int block_size = 10, const int mode = 0);

//! Reconstruct multi-angle TIRF image
/**
   \param h operator (\f$ n_{planes} \times n_{angles} \f$ matrix)
   \param data matirf data with depth = \f$n_{planes}\f$
   \param lambda_xy regularization in xy
   \param lambda_xy regularization in z
   \param gamma time step
   \param max_iter number of iterations
   \param algo algorithm (-1:landweber,0:nnls,1:ppxa,2:ppxa blind, 3:ppxa blind adaptive)
   \param regularization regularization id
   \param p size of the bloc (21x21 by default)
   \param mode  if mode>1 display iterations

   Reconstruction Factory

   the operator is h updated
**/
CImg<> reconstruct_matirf(CImg<> &h, const CImg<> & data,
			  const float lambda_xy, const float lambda_z,
			  const float gamma, const int max_iter,
			  const int algo = 1, const int regularization = 1,
			  const int block_size = 10, const int mode = 0);

//! Reconstruct multi-angle TIRF image (multi index version)
/**
   \param h operator (\f$ n_{planes} \times n_{angles} \f$ matrix)
   \param data matirf data with depth = \f$n_{planes}\f$
   \param lambda_xy regularization in xy
   \param lambda_xy regularization in z
   \param gamma time step
   \param max_iter number of iterations
   \param algo algorithm (-1:landweber,0:nnls,1:ppxa,2:ppxa blind, 3:ppxa blind adaptive)
   \param regularization regularization id
   \param p size of the bloc (21x21 by default)
   \param mode  if mode>1 display iterations

   Reconstruction Factory

   the operator is h updated
**/
CImg<> reconstruct_matirf(const CImgList<> &h, const CImg<int> &map, const CImg<> & data,
			  const float lambda_xy, const float lambda_z,
			  const float gamma, const int max_iter,
			  const int algo = 1, const int regularization = 1,
			  const int block_size = 10, const int mode = 0);

CImg<> reconstruct_matirf(CImgList<> &h, const CImg<int> &map, const CImg<> & data,
			  const float lambda_xy, const float lambda_z,
			  const float gamma, const int max_iter,
			  const int algo = 1, const int regularization = 1,
			  const int block_size = 10, const int mode = 0);

//! Get the string description from the smoother id
const char * get_smoother_str(const int id);

//! Get the string description from the smoother id
const char * get_algorithm_str(const int id);
