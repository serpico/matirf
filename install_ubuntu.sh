#!/bin/bash

echo ----------------
echo Install svdeconv
echo ----------------

# install lib dependencies
echo "install libs"
echo "-----------------"
apt-get update
apt-get upgrade
apt-get -y install cmake
apt-get -y install g++
apt-get -y install libpng-dev
apt-get -y install libtiff-dev
apt-get -y install libfftw3-dev

# make svdeconv
echo Install svdeconv
echo ----------------
mkdir build
cd build
cmake ../
make

# tests
cd tests
ctest
