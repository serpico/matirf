FROM ubuntu:20.04

WORKDIR /app

COPY . /app

RUN apt-get -y update && \
    apt-get -y upgrade && \
    apt-get -y install python3 && \
    apt-get -y install cmake && \
    apt-get -y install g++ && \
    apt-get -y install libboost-all-dev && \
    apt-get -y install libjpeg-dev && \
    apt-get -y install libpng-dev && \
    apt-get -y install libtiff-dev && \
    apt-get -y install libfftw3-dev && \
    mkdir build && \
    cd build && \
    cmake .. && \
    make && \
    cp /app/matirf_sequence.py /app/build/bin/matirf_sequence.py

ENV PATH="/app/build/bin:$PATH"

CMD ["bash"]
